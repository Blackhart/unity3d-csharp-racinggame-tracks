﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class AudioClipManager : MonoBehaviour
{
	[SerializeField]
    public List<string> names = new List<string>();
    [SerializeField]
    public List<AudioClip> clips = new List<AudioClip>();
    private Dictionary<string, AudioClip> m_clips = new Dictionary<string, AudioClip>();
    private AudioSource source;

    private void Start()
    {
        this.source = this.GetComponent<AudioSource>();
        if (this.names.Count != this.clips.Count)
            Debug.LogError("AudioClipManager : list of names and clips not same length");
        else
        {
            for (int i = 0, size = this.clips.Count; i < size; ++i)
            {
                this.m_clips[this.names[i]] = this.clips[i];
		Debug.Log("Added to collection : " + this.names[i] + " for " + this.clips[i].ToString());
            }
        }
    }

    public bool PlayOnce(string clipName)
    {
        Debug.Log("Playing once : " + clipName);
        if (this.m_clips.ContainsKey(clipName) == false)
            return false;
        else
        {
            this.source.PlayOneShot(this.m_clips[clipName]);
            return true;
        }
    }
}
