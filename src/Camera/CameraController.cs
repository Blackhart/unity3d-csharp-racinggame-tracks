﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

	#region Public Members


	public bool inLoop = false;
	public float loopAngle = 20f;
	public GameObject mCar; // Car game object
    public Transform mLookTarget;
    public Transform mCamTarget;
	public float mDistance = 6.4f; // Distance between camera and player (X axis)
	public float mHeight = 10.0f; // Height between terrain and camera (Y axis)
	public float mLookAtDist = 0.0f; // Additional dist between lookat point and player
	public float mRotationDamping = 3.0f; // Step rotation
	public float mFOVDamping = 2.0f; // Step FOV
    public float mDistDamping = 2.0f; // Step dist
	public float mDefaultFOV = 60.0f; // Default FOV 60.0f/70.0f
	public float mRatioZoom = 0.5f; // Multiplier Ratio  fov/unit speed
    public float mRatioDist = 0.5f; // Multiplier Ratio  dist / unit speed
	#endregion

	#region Private Members
	private Controlleur mControlleur;
    private Vector3 mWantedPos;
    private Quaternion mWantedRot;
    private Vector3 mWantedCarPos;
    private float mRealDist;
	#endregion


	void Start()
	{
		mControlleur = mCar.GetComponent<Controlleur>();
		mCamTarget.localPosition = new Vector3(0.0f, mHeight, -mDistance);
        mLookTarget.localPosition = new Vector3(0.0f, 0.0f, mLookAtDist);
		transform.position = mCamTarget.position;
        transform.rotation = mCamTarget.rotation;
        mRealDist = mDistance;
	}

	void LateUpdate()
	{
		UpdateCamTarget();
        mWantedPos = mCamTarget.position;
		if (inLoop)
			Loop(mCamTarget);
		else
			mWantedRot = mCamTarget.rotation;
        mWantedCarPos = mCar.transform.position;
		Quaternion lRot = transform.rotation;
        PosUpdate();
        RotUpdate(lRot);
	}

	void Loop(Transform trans)
	{
		Quaternion tmp = trans.rotation;
		trans.Rotate(Vector3.right * -loopAngle);
		mWantedRot = trans.rotation;
		trans.rotation = tmp;
	}

    void    RotUpdate(Quaternion pRot)
    {
        transform.rotation = Quaternion.Slerp(pRot, mWantedRot, mRotationDamping * Time.deltaTime);
    }

    void    PosUpdate()
    {
        // Local cam pos
        Vector3 lCamPosTmp = mCar.transform.InverseTransformPoint(transform.position);
        
        // Local target pos
        Vector3 lTargetPosTmp = mCar.transform.InverseTransformPoint(mCamTarget.position);

        // Move cam pos to target pos
        Vector3 lPos = Vector3.Slerp(lCamPosTmp, lTargetPosTmp, mRotationDamping * Time.deltaTime);
        lPos.y = Mathf.Lerp(lCamPosTmp.y, lTargetPosTmp.y, mRotationDamping * Time.deltaTime);
        
        // Local car pos
        Vector3 lCarPosTmp = new Vector3(0.0f, mHeight, 0.0f);
        
        // Dir car pos to cam pos
        Vector3 lDirNorm = (lPos - lCarPosTmp).normalized;
        
        // Set new pos
        transform.position = mCar.transform.TransformPoint(lCarPosTmp + lDirNorm * mRealDist);
    }

	void FixedUpdate()
	{
        // Get acceleration
		float acc = mControlleur.currentSpeed;
        
        // Update new magnitude from cam pos to car pos
        mRealDist = Mathf.Lerp(-(mCamTarget.localPosition.z), mDistance + acc * mRatioDist, mDistDamping * Time.deltaTime);
        mCamTarget.localPosition = new Vector3(mCamTarget.localPosition.x, mCamTarget.localPosition.y, -mRealDist);
		
        // Update field of vue about speed
        camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, mDefaultFOV + acc * mRatioZoom, mFOVDamping * Time.deltaTime);
	}

	void UpdateCamTarget()
	{
		float lAngle = 90.0f - (Mathf.Atan(((-mCamTarget.localPosition.z + mLookAtDist) / mHeight)) * Mathf.Rad2Deg);
		mCamTarget.localRotation = Quaternion.Euler(lAngle, 0.0f, 0.0f);
        mCamTarget.localPosition = new Vector3(0.0f, mHeight, -mDistance);
        mLookTarget.localPosition = new Vector3(0.0f, 0.0f, mLookAtDist);
		//Debug.DrawRay(transform.position, transform.forward * 100.0f, Color.green);
	}
}
