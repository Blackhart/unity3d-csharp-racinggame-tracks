﻿using UnityEngine;
using System.Collections;

public class CameraTriggerLoop : MonoBehaviour
{
	public GameObject cam;

	private void OnTriggerEnter(Collider pCol)
	{
		if (pCol.tag == "Player" && !pCol.isTrigger)
		{
			Controlleur lControlleur = pCol.gameObject.GetComponent<Controlleur>();
			if (lControlleur && (lControlleur.photonView.isMine || lControlleur.noNetwork))
			{
				cam.GetComponent<CameraController>().inLoop = !cam.GetComponent<CameraController>().inLoop;
			}
		}
	}
}
