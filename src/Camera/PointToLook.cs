﻿using UnityEngine;
using System.Collections;

public class PointToLook : MonoBehaviour 
{
    public float distanceLevel = 100;
	public GameObject player;
	public float distancePlayer = 0;
	public float distanceParcouru;
	
	// Use this for initialization
	void Start () 
	{
		if (this.distanceLevel == 0.0f)
			this.distanceLevel = 1.0f;
		distanceParcouru = distancePlayer;
		transform.position = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), this.distanceParcouru / distanceLevel);
	}
	
	// Update is called once per frame
	void Update () 
	{
		float tmp;
		
		if (this.player)
		{
			tmp = this.getDistance();
			this.distanceParcouru += (tmp / 100);
			if (this.distanceParcouru < 0)
				this.distanceParcouru = 0;
			if (this.distanceParcouru > this.distanceLevel)
				this.distanceParcouru = this.distanceLevel;
			transform.position = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), this.distanceParcouru / distanceLevel);
		}
	}
	
	private float getDistance()
	{
		Vector3 next;
		Vector3 last;
		Vector3 tmpPlayer;
		float angle;
		float distance;
		float ret;
		
		next = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), (this.distanceParcouru + 1f) / distanceLevel);
		last = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), this.distanceParcouru / distanceLevel);
		tmpPlayer = player.transform.position;
		tmpPlayer.y = last.y;
		next = next - last;
		last = tmpPlayer - last;
		angle = Vector3.Angle(next, last) * Mathf.Deg2Rad;
		distance = Mathf.Abs(Vector3.Distance(transform.position, tmpPlayer));
		ret = Mathf.Cos(angle) * distance;
		return ret;
	}
}
	