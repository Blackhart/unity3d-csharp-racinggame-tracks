﻿using UnityEngine;
using System.Collections;

public class TriggerNear : MonoBehaviour 
{
	public float	nextNear;
	public float	lastNear;

	private Camera	cam;


	// Use this for initialization
	void Start () 
	{
		cam = Camera.main;
	}

	void OnTriggerEnter(Collider col)
	{
		Controlleur	cont;
		float		angle;

		if (cam == null)
			return;
		if (col.CompareTag("Player") == true)
		{
			cont = col.gameObject.GetComponent<Controlleur>();
			if (cont != null && (cont.photonView.isMine || cont.noNetwork))
			{
				angle = Vector3.Angle(col.transform.forward, this.transform.forward);
				if (angle > 0.0f && angle <= 90.0f)
					cam.nearClipPlane = lastNear;
				else
					cam.nearClipPlane = nextNear;
			}
		}
	}
}
