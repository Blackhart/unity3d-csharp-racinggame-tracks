﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour
{
	//public Animator animator;
	public Animation anim;
	public Transform platform;
	public Controlleur controller;
	public Filin filin;
	public Turbo turbo;
	public bool hit = false;
	public bool grap = false;
	public PlateformeInfos platformInfos;

	private eFilinState lastState;
	private bool firstPassage = false;
	private bool firstGrap = false;

	private void Start()
	{
		if (this.filin != null)
			this.lastState = this.filin._filinState;
		this.controller = this.GetComponent<Controlleur>();
		if (this.controller.photonView.isMine == true)
		{
			this.platformInfos.InitPlayer(this.controller);
		}
	}

	private void FixedUpdate()
	{
		this.PlayAnim();
	}

	public void PlayAnim()
	{
		if (this.anim == null)
			return;
		if (this.hit == true)
		{
			if (this.anim.IsPlaying("Hit") == false && this.firstPassage == true)
			{
				this.hit = false;
				this.firstPassage = false;
			}
			else
			{
				this.firstPassage = true;
				this.anim.CrossFade("Hit");
				return;
			}
		}
		/*if (this.grap == true)
		{
			if (this.anim.IsPlaying("Grap") == false && this.firstGrap == true)
			{
				this.firstGrap = false;
				this.grap = false;
			}
			else
			{
				this.anim.CrossFade("Grap");
				this.firstGrap = true;
				return;
			}
		}*/
		EState state = this.controller.state;
		if (this.platform != null && this.filin != null)
		{
			if (Vector3.Distance(this.transform.position, this.platform.position) <= this.filin.m_maxDist)
			{
				if (state == EState.e_LTurn)
				{
					this.anim.CrossFade("Drone_Lturn");
				}
				else if (state == EState.e_RTurn)
				{
					this.anim.CrossFade("Drone_Rturn");
				}
				else
				{
					this.anim.CrossFade("NearDrone");
				}
				if (this.platformInfos != null)
				{
					if (this.filin._filinState == eFilinState._ACTIVE_)
					{
						this.platformInfos.downloadOn.SetActive(true);
						this.platformInfos.downloadOff.SetActive(false);
					}
					else
					{
						this.platformInfos.downloadOn.SetActive(false);
						this.platformInfos.downloadOff.SetActive(true);
					}
				}
				return;
			}
			else
			{
				if (this.platformInfos != null)
				{
					this.platformInfos.downloadOff.SetActive(false);
					this.platformInfos.downloadOn.SetActive(false);
				}
			}
		}
		switch (state)
		{
			case EState.e_Jump:
				//this.animation.CrossFadeQueued("Jump", 0.3f, QueueMode.CompleteOthers);
				this.anim.CrossFade("Jump");
				return;
			case EState.e_LTurn:
				//this.animation.CrossFadeQueued("Lturn", 0.3f, QueueMode.CompleteOthers);
				this.anim.CrossFade("Lturn");
				return;
			case EState.e_RTurn:
				//this.animation.CrossFadeQueued("Rturn", 0.3f, QueueMode.CompleteOthers);
				this.anim.CrossFade("Rturn");
				return;
			default:
				break;
		}
		if (this.controller.photonView.isMine == true || this.controller.noNetwork)
		{
			if (this.turbo.m_turbo == true)
			{
				this.anim.CrossFade("Turbo");
				return;
			}
			if (this.controller.currentSpeed >= this.controller.maxSpeed * 0.8f)
			{
				this.anim.CrossFade("Maxspeed");
				return;
			}
		}
		this.anim.CrossFade("Idle");
		//this.animator.SetBool("Grab", this.filin._filinState != this.lastState && this.filin._filinState == eFilinState._ACTIVE_);
		//this.lastState = this.filin._filinState;
	}

	/*public void PlayAnim()
	{
		if (this.animator == null)
			return;
		EState state = this.controller.state;
		switch (state)
		{
			case EState.e_Jump:
				this.animator.SetBool("Jump", true);
				break;
			case EState.e_LTurn:
				this.animator.SetBool("LTurn", true);
				this.animator.SetBool("RTurn", false);
				break;
			case EState.e_RTurn:
				this.animator.SetBool("RTurn", true);
				this.animator.SetBool("LTurn", false);
				break;
			default:
				this.animator.SetBool("RTurn", false);
				this.animator.SetBool("LTurn", false);
				this.animator.SetBool("Jump", false);
				break;
		}

		if (this.controller.photonView.isMine == true || this.controller.noNetwork)
		{
			this.animator.SetBool("MaxSpeed", this.controller.currentSpeed >= (this.controller.maxSpeed * 0.8f));
		}
		if (this.platform != null && this.filin != null)
			this.animator.SetBool("NearDrone", Vector3.Distance(this.transform.position, this.platform.position) <= this.filin.m_maxDist);
		//this.animator.SetBool("Grab", this.filin._filinState != this.lastState && this.filin._filinState == eFilinState._ACTIVE_);
		//this.lastState = this.filin._filinState;
	}*/
}
