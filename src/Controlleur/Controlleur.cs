﻿using UnityEngine;
using System.Collections;

public class Controlleur : Photon.MonoBehaviour
{
	public float minSpeed;
	public float maxSpeed;
	public float acceleration;
	public float decceleration;
	public float dragForce;
	public float currentSpeed;
	public int rotateSpeedMax;
	public float angleRotation;
	public Transform rail;
	public bool noNetwork = false;
	public float forceDash;
	public float consoDash;
	public string eventName;
	public FmodEventAsset asset;

	public Vector3 curAngles;
	public float boostSpeed;
	public GameObject body;
	public EState state
	{
		get { return _state; }
		set { _state = value; }
	}
	private EState _state;
	private float _speedPlateform;

	public FmodEventAudioSource source;
	private FXManager fxManager;
    private DJLoup themeSound;
	private bool collision = false;

	void Awake()
	{
		if (Moteur.CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
		else
		{
			this.source.playOnAwake = false;
		}
		if (GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().network == true && GameObject.Find("Network") == null)
		{
			Transform tmp = this.transform.FindChild("Hoverboard").parent;
			Destroy(this.transform.FindChild("Hoverboard").gameObject);
			GameObject obj = GameObject.Instantiate(Resources.Load(GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().model + "/Hoverboard"), this.transform.position, this.transform.rotation) as GameObject;
			obj.transform.parent = tmp;
			Vector3 vec = new Vector3(0.0f, this.transform.eulerAngles.y, 0.0f);
			obj.transform.localEulerAngles = vec;
			this.body = obj;
			this.GetComponent<AnimationController>().anim = obj.transform.FindChild(GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().model).gameObject.GetComponent<Animation>();
			Camera.main.GetComponent<CameraController>().mCar = this.gameObject;
		}
	}

	// Use this for initialization
	void Start () 
	{
		this.fxManager = this.GetComponent<FXManager>();
		this.rigidbody.freezeRotation = true;
		curAngles = Vector3.zero;
		curAngles.y = this.calculeAngleWorld();
		transform.eulerAngles = this.curAngles;
        themeSound = GameObject.Find("DJLoup").GetComponent<DJLoup>();
        themeSound.launchGameThemeSong();
	}

	public void Init()
	{
		curAngles = Vector3.zero;
		curAngles.y = this.calculeAngleWorld();
		transform.eulerAngles = this.curAngles;
		if (noNetwork)
		{
			this.GetComponent<Filin>().Init();
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (photonView.isMine && GameLogic.isRaceStart || noNetwork)
		{
			this.DirectionUpdate();
		}
	}

	void FixedUpdate()
	{
		if (photonView.isMine && GameLogic.isRaceStart || noNetwork)
		{
			this.CalculateSpeed();
			this.Dash();
			if (this.rail == null)
			{
				float steerForce = Input.GetAxis("Horizontal") * rotateSpeedMax;
				if (steerForce > 0.5)
					this.state = EState.e_RTurn;
				else if (steerForce < -0.5f)
					this.state = EState.e_LTurn;
				else if (this.state == EState.e_Jump)
					this.state = EState.e_Jump;
				else
					this.state = EState.e_iddle;
				Quaternion rotation = Quaternion.Euler(Vector3.up * steerForce);
				this.rigidbody.MoveRotation(this.rigidbody.rotation * rotation);
			}
		}
	}

	void CalculateSpeed()
	{
		float fwdForce = Input.GetAxis("Forward") * acceleration;

		fwdForce = Mathf.Max(0, fwdForce);
		if (fwdForce == 0)
		{
			this.currentSpeed += minSpeed - decceleration;
			if (this.checkFilin())
				this.currentSpeed = Mathf.Max(this.currentSpeed, _speedPlateform);
		}
		else
			this.currentSpeed += minSpeed + fwdForce;
		if (this.currentSpeed < minSpeed)
			this.currentSpeed = minSpeed;
		if (this.currentSpeed > this.maxSpeed)
			this.currentSpeed = this.maxSpeed;
		this.currentSpeed += boostSpeed;
		if (this.collision)
			this.currentSpeed -= this.dragForce;
		this.rigidbody.AddForce(transform.forward * currentSpeed * 10, ForceMode.Force);
	}

	void OnCollisionEnter(Collision other)
	{
		this.rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		if (other.gameObject.CompareTag("Player"))
			this.source.Play();
	}

	void OnCollisionStay(Collision other)
	{
		if (other.contacts[0].normal.x < 0.0f)
		{
			if (this.transform.forward.z > 0.0f)
				this.fxManager.collisionLeft.particleSystem.Play();
			else
				this.fxManager.collisionRight.particleSystem.Play();
		}
		if (other.contacts[0].normal.x > 0.0f)
		{
			if (this.transform.forward.z > 0.0f)
				this.fxManager.collisionRight.particleSystem.Play();
			else
				this.fxManager.collisionLeft.particleSystem.Play();
		}
	}

	void OnCollisionExit(Collision other)
	{
		this.rigidbody.freezeRotation = true;
		this.fxManager.collisionLeft.particleSystem.Stop();
		this.fxManager.collisionRight.particleSystem.Stop();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Obstacle"))
		{
			this.fxManager.collisionFront.particleSystem.loop = false;
			this.fxManager.collisionFront.particleSystem.Play();
		}
	}

	void OnTriggerExit(Collider other)
	{
		this.fxManager.collisionFront.particleSystem.Stop();
	}

	void DirectionUpdate()
	{
		if (this.rail != null)
		{
			float change = this.rotateSpeedMax * Input.GetAxis("Horizontal") * Time.deltaTime;
			this.DirectionUpdate(change);
		}
	}

	void DirectionUpdate(float value)
	{
		float angleWorld = this.calculeAngleWorld();
		if (angleWorld < 0)
			angleWorld += 360;
		float diff = angleWorld - this.angleRotation;
		float tmpAngle;

		if (transform.up.y < 0)
			value *= -1;
		this.curAngles = transform.eulerAngles;
		this.curAngles.y += value;

		if (diff < 0)
		{
			tmpAngle = this.curAngles.y - diff;
			if (tmpAngle >= 355) // a verifier pour plus de precision
				tmpAngle -= 360;
			tmpAngle = Mathf.Clamp(tmpAngle, 0, this.angleRotation * 2);
			this.curAngles.y = tmpAngle + diff;
		}
		else
			this.curAngles.y = Mathf.Clamp(this.curAngles.y, angleWorld - this.angleRotation, this.angleRotation + angleWorld);
		transform.eulerAngles = this.curAngles;
	}

	float calculeAngleWorld()
	{
		Vector3 position;
		Vector3 end;
		float distance;
		float angle;

		if (this.rail == null)
			return 0f;
		distance = this.rail.GetComponent<PointToLook>().distanceParcouru;
		if (distance < 0)
			distance = 0;
		position = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), distance / this.rail.GetComponent<PointToLook>().distanceLevel);
		if (this.rail.GetComponent<PointToLook>().distanceLevel < distance + 1f)
			end = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), (this.rail.GetComponent<PointToLook>().distanceLevel - 1) / this.rail.GetComponent<PointToLook>().distanceLevel);
		else
			end = iTween.PointOnPath(iTweenPath.GetPath("PointToLook"), (distance + 1f) / this.rail.GetComponent<PointToLook>().distanceLevel);
		end = end - position;
		angle = Vector3.Angle(Vector3.forward.normalized, end.normalized);
		if (end.x > 0)
			return angle;
		return -angle;
	}

	void Dash()
	{
		if (Input.GetButtonDown("Dash Left"))
			this.MakeDash(-transform.right);
		if (Input.GetButtonDown("Dash Right"))
			this.MakeDash(transform.right);
	}

	public void setBoostSpeed(float value)
	{
		boostSpeed = value;
	}

	private void MakeDash(Vector3 direction)
	{
		Jauge tmp = this.GetComponent<Jauge>();

		if (tmp != null && consoDash < tmp._jauge)
		{
			this.rigidbody.AddForce(direction * forceDash, ForceMode.Impulse);
			tmp.m_jauge = tmp._jauge - consoDash;
		}
	}

	private bool checkFilin()
	{
		Filin fil = this.GetComponent<Filin>();
		movePlateform plateform;

		if (fil == null)
			return false;
		if (fil.getPlayer() == this.gameObject)
		{
			plateform = fil.m_target.GetComponent<movePlateform>();
			if (plateform == null || !plateform.isLaunch)
				return false;
			_speedPlateform = fil.m_target.GetComponent<movePlateform>().speedPlateforme;
			return true;
		}
		return false;
	}
}
