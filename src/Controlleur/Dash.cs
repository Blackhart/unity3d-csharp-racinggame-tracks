﻿using UnityEngine;
using System.Collections;

public class Dash : MonoBehaviour {
	
	#region Private members
//	private	CustomCharacterController	_controller;
	private	Jauge						_jauge;
	private	bool[]						_state = { false, false, false };
	private	bool						_hasDashing;
	private	float						_traveledDist;
	#endregion
	
	#region Public members
	public	float	m_dashDistSpeed; // traveled distance/s
	public	float	m_dashDist; // traveled distance
	public	float	m_dashConso; // jauge conso/action
	#endregion
	
	// Use this for initialization
	void Start () {
//		_controller = GetComponent<CustomCharacterController>();
		_jauge = GetComponent<Jauge>();
		_hasDashing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!_hasDashing)
			setState ();
		if (_hasDashing) {
			if (_state[0])
				dash(transform.TransformDirection(Vector3.left));
			else if (_state[1])
				dash(transform.TransformDirection(Vector3.right));
			else if (_state[2])
				dash(transform.TransformDirection(Vector3.forward));
		}
	}
	
	private	void	dash(Vector3 pDir) 
	{
		float	lcurTraveledDist =  m_dashDistSpeed * Time.deltaTime;

		_traveledDist += lcurTraveledDist;
		if (_traveledDist > m_dashDist)
			lcurTraveledDist -= _traveledDist - m_dashDist;
		if (_traveledDist >= m_dashDist) {
			_hasDashing = false;
			_jauge.m_jauge -= m_dashConso;
			_traveledDist = 0.0f;
		}
		//GetComponent<Rigidbody>().MovePosition(transform.position + pDir * lcurTraveledDist);
	}
	
	private	void	setState() {
		_state[0] = Input.GetButtonDown("Dash Left");
        _state[1] = Input.GetButtonDown("Dash Right");
        _state[2] = Input.GetButtonDown("Dash Forward");
		if ((_state[0] || _state[1] || _state[2]) && _jauge.m_jauge >= m_dashConso)
			_hasDashing = true;
	}
}
