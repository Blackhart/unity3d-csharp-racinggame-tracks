﻿using UnityEngine;
using System.Collections;

public class FXManager : MonoBehaviour
{
	public GameObject	collisionLeft;
	public GameObject	collisionRight;
	public GameObject	collisionFront;
	public GameObject	discharge;
	public GameObject	grabCollectiblePart1;
	public GameObject	grabCollectiblePart2;
	public GameObject	turbo;

	// Use this for initialization
	void Start()
	{
		this.collisionLeft.particleSystem.Stop();
		this.collisionRight.particleSystem.Stop();
		this.collisionFront.particleSystem.Stop();
		this.discharge.particleSystem.Stop();
		this.grabCollectiblePart1.particleSystem.Stop();
		this.grabCollectiblePart2.particleSystem.Stop();
		this.turbo.SetActive(false);
	}
}
