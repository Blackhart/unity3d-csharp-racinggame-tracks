﻿using UnityEngine;
using System.Collections;

public class Turbo : MonoBehaviour
{

	#region Private members
	private float _turboSpeed;
	public float m_turboSpeed
	{
		get
		{
			return _turboSpeed;
		}
		set
		{
			_turboSpeed = Mathf.Clamp(value, 0.0f, m_turboSpeedMax);
		}
	}

	private bool _turbo;
	public bool m_turbo
	{
		get;
		private set;
	}

	private Jauge _jauge;
	private FXManager fxManager;
	#endregion

	#region Public members
	public float m_turboSpeedMax; // Max speed turbo
	public float m_turboAcc; // Turbo speed acceleration/s
	public float m_turboDec; // Turbo speed deceleration/s
	public float m_turboConso; // Consomation turbo/s
	public FmodEventAudioSource source;
	public string eventName;
	public FmodEventAsset asset;
	public Controlleur controlleur;
	#endregion

	void Awake()
	{
		if (Moteur.CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
		else
		{
			this.source.playOnAwake = false;
		}
	}

	// Use this for initialization
	void Start()
	{
		_jauge = GetComponent<Jauge>();
		m_turboSpeed = 0.0f;
		m_turbo = false;
		this.controlleur = this.GetComponent<Controlleur>();
		this.fxManager = this.GetComponent<FXManager>();
	}

	// Update is called once per frame
	void Update()
	{
		if (this.controlleur.photonView.isMine && GameLogic.isRaceStart || this.controlleur.noNetwork)
		{
			turbo();
		}
	}

	private void turbo()
	{
		// Get turbo state button
		if (Input.GetButtonDown("Turbo") == true)
		{
			m_turbo = true;
		}
		else if (Input.GetButtonUp("Turbo") == true)
		{
			m_turbo = false;
		}

		if (m_turbo == true)
		{
			this.fxManager.turbo.SetActive(true);
			this.source.Play();
		}
		else
		{
			this.fxManager.turbo.SetActive(false);
			if (this.source.getStatus() != "Stopped")
			{
				this.source.Stop();
			}
		}
		float lTime = Time.deltaTime;

		// Calculate acceleration and deceleration
		if (m_turbo)
			turboAcceleration(lTime);
		if (!m_turbo)
			turboDeceleration(lTime);

		// Set speedTurbo in CharacterController
		this.gameObject.GetComponent<Controlleur>().setBoostSpeed(m_turboSpeed);
	}

	private void turboAcceleration(float pTime)
	{
		float lConso = m_turboConso * pTime;
		if (_jauge.m_jauge - lConso < 0.0f)
		{
			m_turbo = false;
			return;
		}
		m_turboSpeed += m_turboAcc * pTime;
		_jauge.m_jauge -= lConso;
	}

	private void turboDeceleration(float pTime)
	{
		m_turboSpeed -= m_turboDec * pTime;
	}
}
