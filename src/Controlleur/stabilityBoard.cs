﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class stabilityBoard : MonoBehaviour
{
    public float floatSpeed = 2f;
    public float hoverboardHeight;
    public float lerpAngle = 0.5f;
	public float forceJump = 10f;
	public float forceDown = 10f;
	public float anglePente = 45f;
	public string nameRoad = "Ground";

	private Controlleur control;
	private bool onGround = false;


	void Start()
	{
		this.control = this.GetComponent<Controlleur>();
	}

    void FixedUpdate()
    {
        this.AlignBoard();
		this.HoverEffect();
		this.GravityDown();
		this.Jump();
    }
	
    void AlignBoard()
    {
        RaycastHit hit;

		if (Physics.Raycast(transform.position, -transform.up, out hit) && hit.collider.CompareTag(nameRoad))
		{
			Quaternion rot = Quaternion.FromToRotation(transform.up, hit.normal) * this.rigidbody.rotation;
			//if (Quaternion.Angle(this.rigidbody.rotation, rot) < anglePente)
			this.rigidbody.MoveRotation(Quaternion.Lerp(this.rigidbody.rotation, rot, lerpAngle));
		}
		else
		{
			Quaternion rot = this.rigidbody.rotation;
			Vector3 tmp = rot.eulerAngles;
			tmp.z = 0;
			rot.eulerAngles = tmp;
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rot, lerpAngle);
		}
    }

    void HoverEffect()
    {
        RaycastHit hit;
		float diffDistance;

        if (Physics.Raycast(transform.position, -transform.up, out hit, hoverboardHeight))
        {

			if (control != null && control.currentSpeed < 10f)
			{
				this.rigidbody.AddForce(this.transform.up * floatSpeed, ForceMode.Force);
			}
			else
			{
				diffDistance = hoverboardHeight - hit.distance;
				this.transform.Translate(Vector3.up * diffDistance);
			}
			this.gameObject.GetComponent<Controlleur>().state = EState.e_iddle;
			this.onGround = true;
        }
        else
            this.onGround = false;
    }

	void Jump()
	{
		if (Input.GetButtonDown("Jump") == true && this.onGround)
		{
            this.rigidbody.AddForce(this.transform.up * forceJump, ForceMode.VelocityChange);
			this.onGround = false;
			this.gameObject.GetComponent<Controlleur>().state = EState.e_Jump;
		}
	}

	void GravityDown()
	{
		RaycastHit hit;

		if (this.onGround == false)
		{
			if (Physics.Raycast(transform.position, -transform.up, out hit) && hit.collider.CompareTag(nameRoad))
				this.rigidbody.AddForce(-transform.up * forceDown, ForceMode.Acceleration);
		}
	}
}
