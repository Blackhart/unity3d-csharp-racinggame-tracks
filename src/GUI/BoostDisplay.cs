﻿using UnityEngine;
using System.Collections;

public class BoostDisplay : MonoBehaviour
{
	public UISprite[] sprites;
	//public UISlider[] sliders;
	//public UISprite[] foregrounds;
	public float currentValue;

	private Jauge localPlayer;
	private bool init = false;

	private void Start()
	{
		this.Init();
	}

	public void Init()
	{
		GameObject network = GameObject.Find("Network");
		if (network)
		{
			GameLogic gameLogic = network.GetComponent<GameLogic>();
			if (gameLogic)
			{
				GameObject localPlayer = gameLogic.Player;
				if (localPlayer == null)
					this.enabled = false;
				else
					this.localPlayer = localPlayer.GetComponent<Jauge>();
			}
		}
		else
		{
			GameObject localPlayer = GameObject.Find("Player");
			if (localPlayer == null)
				this.enabled = false;
			else
				this.localPlayer = localPlayer.GetComponent<Jauge>();
		}
		this.init = true;
	}

	private void Update()
	{
		if (this.init)
		{
			this.currentValue = this.localPlayer.m_jauge;
			this.UpdateCoeff();
		}
	}

	private void UpdateCoeff()
	{
		int full = Mathf.FloorToInt(this.currentValue / 10);

		if (this.currentValue > 0)
		{
			if (full >= this.sprites.Length)
				full = this.sprites.Length - 1;
			for (int i = 0; i < full; ++i)
			{
				this.sprites[i].fillAmount = 1;
				//this.sliders[i].value = 1;
				//this.foregrounds[i].color = Color.Lerp(Color.red, Color.green, Mathf.FloorToInt(this.currentValue / 10) * 10 / this.localPlayer.m_maxJaugeValue);
			}
			this.sprites[full].fillAmount = this.currentValue / 10 - full;
			//this.sliders[full].foregroundWidget.enabled = true;
			//this.sliders[full].value = this.currentValue / 10 - full;
			//this.foregrounds[full].color = Color.Lerp(Color.red, Color.green, Mathf.FloorToInt(this.currentValue / 10) * 10 / this.localPlayer.m_maxJaugeValue);

			for (int i = full + 1, size = this.sprites.Length; i < size; ++i)
			{
				this.sprites[i].fillAmount = 0;
				//this.sliders[i].value = 0;
			}
		}
		else
		{
			for (int i = 0, size = this.sprites.Length; i < size; ++i)
			{
				this.sprites[i].fillAmount = 0;
				//this.sliders[i].value = 0;
			}
		}
	}
}
