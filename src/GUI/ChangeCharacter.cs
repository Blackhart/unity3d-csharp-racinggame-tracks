﻿using UnityEngine;
using System.Collections;

public class ChangeCharacter : MonoBehaviour
{
	public UISprite[] lockedImages;
	public UISprite[] parentImages;
	public MoveCadenas cadenas;

	private int current = -1;

	void Start()
	{
		for (int i = 0; i < this.lockedImages.Length; i++)
		{
			this.parentImages[i].height = Mathf.FloorToInt(Screen.height / 1.4f);
			this.parentImages[i].leftAnchor.absolute = (Screen.width >> 1) + this.parentImages[i].width * (i - 1);
			this.parentImages[i].rightAnchor.absolute = this.parentImages[i].leftAnchor.absolute + this.parentImages[i].width;
		}
	}

	public void UpdateCharacterSelection(int index)
	{
		if (index == -1)
		{
			this.cadenas.UpdateCharacterLocked(null);
			if (this.current != -1)
			{
				this.lockedImages[this.current].gameObject.SetActive(false);
			}
		}
		else
		{
			if (this.current != index)
			{
				if (this.current != -1)
				{
					this.lockedImages[this.current].gameObject.SetActive(false);
				}
				this.lockedImages[index].gameObject.SetActive(true);
				this.cadenas.UpdateCharacterLocked(this.parentImages[index]);
			}
		}
		this.current = index;
	}
}
