﻿using UnityEngine;
using System.Collections;

public class Chrono : MonoBehaviour
{
	private System.DateTime current;
	private System.TimeSpan diff;
	public bool started = false;
	public UILabel label;

	// Use this for initialization
	void Start()
	{
		if (GameObject.Find("Player").GetComponent<Controlleur>().noNetwork == true)
		{
			this.current = System.DateTime.Now;
			this.started = true;
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (this.started == true)
		{
			this.diff += System.DateTime.Now - this.current;
			this.current = System.DateTime.Now;
			this.label.text = string.Format("{0:D2}:{1:D2}:{2:D2}", this.diff.Minutes, this.diff.Seconds, this.diff.Milliseconds);
		}
	}

	public void ResetTimer()
	{
		this.current = System.DateTime.Now;
		this.diff = System.TimeSpan.Zero;
	}
}
