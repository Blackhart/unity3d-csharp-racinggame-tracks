﻿using UnityEngine;
using System.Collections;

public class FadingText : MonoBehaviour
{
	public UILabel label;

	private void Start()
	{
		this.label.alpha = 0f;
	}

	public void Set(string text, float duration)
	{
		this.label.text = text;
		this.label.alpha = 1f;
		TweenAlpha.Begin(this.gameObject, duration, 0f);
	}
}
