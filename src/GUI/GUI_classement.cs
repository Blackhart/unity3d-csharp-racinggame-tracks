﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GUI_classement : MonoBehaviour
{
	#region Public Members
	[HideInInspector]
	public static Dictionary<string, int> mClassement;
	[HideInInspector]
	public GameObject player;
	#endregion

	#region Init
	private void Start()
	{
		mClassement = new Dictionary<string, int>();
	}
	#endregion

	public void Init()
	{
		GameObject[] lPlayers = GameObject.FindGameObjectsWithTag("Player");

		foreach (GameObject lObject in lPlayers)
		{
			if (mClassement.ContainsKey("Player " + lObject.GetComponent<NetworkCharacter>().ID) == false)
			{
				mClassement.Add("Player " + lObject.GetComponent<NetworkCharacter>().ID, (int)lObject.GetComponent<Filin>().m_score);
			}
			if (lObject.GetPhotonView().isMine)
			{ 
				this.player = lObject;
			}
		}
	}

	#region Unity Update
	private void Update()
	{
		if (GameLogic.isRaceStart)
		{
			mClassement["Player " + this.player.GetComponent<NetworkCharacter>().ID] = (int)this.player.GetComponent<Filin>().m_score;
			mClassement = mClassement.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
			//foreach (KeyValuePair<string, int> lPair in mClassement)
			//	Debug.Log(lPair.Key + ": " + lPair.Value);
		}
	}
	#endregion
}
