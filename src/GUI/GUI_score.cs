﻿using UnityEngine;
using System.Collections;

public class GUI_score : Photon.MonoBehaviour {
	
	#region Public variables
	public	GUIText		m_score = null;
	#endregion
	
	#region Private Variables
	private	GameLogic	_gameLogic = null;
	#endregion
	
	// Initialize
	public void	init()
	{
		GameObject	lNetwork = GameObject.Find("Network");
		if (lNetwork)
			_gameLogic = lNetwork.GetComponent<GameLogic>();
	}
	
	// GUI
	void	OnGUI()
	{
		if (_gameLogic)
		{
			if (_gameLogic.Player)
			{
				Filin	lFilin = _gameLogic.Player.GetComponent<Filin>();
				if (lFilin && m_score)
					m_score.text = "" + (int)lFilin.m_score;
			}
		}
		else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>().noNetwork)
		{
			Filin	lFilin = GameObject.FindGameObjectWithTag("Player").GetComponent<Filin>();
			if (lFilin && m_score)
				m_score.text = "" + (int)lFilin.m_score;
		}
	}
}
