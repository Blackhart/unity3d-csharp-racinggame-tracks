﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour
{
	public UISprite background;
	public float timeNeeded;

	private float timer;

	// Use this for initialization
	void Start()
	{
		this.timer = 0.0f;
	}

	public bool IsLoaded()
	{
		bool ret = false;

		this.timer += Time.deltaTime;
		if (this.timer >= this.timeNeeded)
		{
			ret = true;
		}

		return ret;
	}

	public void ResetTimer()
	{
		this.timer = 0.0f;
	}
}
