﻿using UnityEngine;
using System.Collections;

public class MoveArrow : MonoBehaviour
{
	public UISprite arrow;
	public int offsetX;
	public int offsetY;
	public bool vertical;
	public bool hideOnClick;
	
	void OnHover(bool isOver)
	{
		if (isOver == true)
		{
			this.arrow.topAnchor.target = this.transform;
			this.arrow.bottomAnchor.target = this.transform;
			this.arrow.rightAnchor.target = this.transform;
			this.arrow.leftAnchor.target = this.transform;

			if (this.vertical == true)
			{
				this.arrow.leftAnchor.absolute = (this.GetComponent<UISprite>().width >> 1) - (this.arrow.width >> 1);
			}
			else
			{
				this.arrow.leftAnchor.absolute = this.offsetX;
			}
			this.arrow.rightAnchor.absolute = this.arrow.leftAnchor.absolute + this.arrow.width;
			this.arrow.topAnchor.absolute = (this.arrow.height >> 1) + this.offsetY;
			this.arrow.bottomAnchor.absolute = this.arrow.topAnchor.absolute - this.arrow.height;
			this.arrow.gameObject.SetActive(true);
		}
		else
		{
			this.arrow.gameObject.SetActive(false);
		}
	}

	void OnSelect(bool selected)
	{
		if (selected == true)
		{
			this.arrow.topAnchor.target = this.transform;
			this.arrow.bottomAnchor.target = this.transform;
			this.arrow.rightAnchor.target = this.transform;
			this.arrow.leftAnchor.target = this.transform;

			if (this.vertical == true)
			{
				this.arrow.leftAnchor.absolute = (this.GetComponent<UISprite>().width >> 1) - (this.arrow.width >> 1);
			}
			else
			{
				this.arrow.leftAnchor.absolute = this.offsetX;
			}
			this.arrow.rightAnchor.absolute = this.arrow.leftAnchor.absolute + this.arrow.width;
			this.arrow.topAnchor.absolute = (this.arrow.height >> 1) + this.offsetY;
			this.arrow.bottomAnchor.absolute = this.arrow.topAnchor.absolute - this.arrow.height;
			this.arrow.gameObject.SetActive(true);
		}
		else
		{
			this.arrow.gameObject.SetActive(false);
		}
	}

	void OnClick()
	{
		if (this.hideOnClick)
			this.arrow.gameObject.SetActive(false);
	}
}
