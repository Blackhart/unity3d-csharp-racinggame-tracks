﻿using UnityEngine;
using System.Collections;

public class MoveCadenas : MonoBehaviour
{
	public UISprite cadenas;
	public float speedOpacite;
	public float speedSize;

	private int realWidth;
	private int realHeight;
	private bool changed = false;
	private float currentSize;
	private UISprite currentCharacter;

	void Start()
	{
		this.cadenas.alpha = 0.0f; 
		this.cadenas.width = Mathf.FloorToInt(Screen.width / 10);
		this.cadenas.leftAnchor.absolute = Screen.width >> 1;
		this.cadenas.rightAnchor.absolute = this.cadenas.leftAnchor.absolute - this.cadenas.width;
		this.realHeight = this.cadenas.height;
		this.realWidth = this.cadenas.width;
	}

	public void UpdateCharacterLocked(UISprite image)
	{
		this.currentCharacter = image;
		if (this.currentCharacter == null)
		{
			this.cadenas.gameObject.SetActive(false);
		}
		else
		{
			this.cadenas.alpha = 0.0f;
			this.cadenas.width = Mathf.FloorToInt(this.cadenas.width * 1.2f);
			this.currentSize = this.cadenas.width;
			this.cadenas.leftAnchor.absolute = this.currentCharacter.rightAnchor.absolute - this.cadenas.width;
			this.cadenas.rightAnchor.absolute = this.cadenas.leftAnchor.absolute + this.cadenas.width;
			this.cadenas.bottomAnchor.absolute = this.currentCharacter.bottomAnchor.absolute;
			this.cadenas.topAnchor.absolute = this.cadenas.bottomAnchor.absolute + this.cadenas.height;
			this.cadenas.gameObject.SetActive(true);
			this.changed = true;
		}
	}

	void Update()
	{
		if (this.changed == true)
		{
			if (this.AnimLock() == true)
			{
				this.changed = false;
			}
		}
	}

	bool AnimLock()
	{ 
		bool ret = false;

		this.cadenas.alpha += this.speedOpacite * Time.deltaTime;
		if (this.cadenas.alpha > 1.0f)
			this.cadenas.alpha = 1.0f;
		this.currentSize -= this.speedSize * Time.deltaTime;
		this.cadenas.width = Mathf.FloorToInt(this.currentSize);
		this.cadenas.leftAnchor.absolute = this.currentCharacter.rightAnchor.absolute - this.cadenas.width;
		this.cadenas.rightAnchor.absolute = this.cadenas.leftAnchor.absolute + this.cadenas.width;
		this.cadenas.bottomAnchor.absolute = this.currentCharacter.bottomAnchor.absolute;
		this.cadenas.topAnchor.absolute = this.cadenas.bottomAnchor.absolute + this.cadenas.height;
		if (this.cadenas.width < this.realWidth)
			this.cadenas.width = this.realWidth;
		if (this.cadenas.alpha >= 1.0f && this.cadenas.width == this.realWidth)
		{
			ret = true;
		}

		return ret;
	}
}