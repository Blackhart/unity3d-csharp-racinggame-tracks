﻿using UnityEngine;
using System.Collections;

public class PlateformeInfos : MonoBehaviour
{
	public GameObject downloadOff;
	public GameObject downloadOn;
	public GameObject droneBehind;

	private float distanceParcourue;
	private movePlateform platform;
	private Controlleur player;

	void Start()
	{
		this.platform = GameObject.Find("plateforme").GetComponent<movePlateform>();
	}

	public void InitPlayer(Controlleur player)
	{
		this.player = player;
	}

	void Update()
	{
		float dist;

		Vector3 closestPoint = Vector3.zero;
		iTween.ClosestPointOnPath(iTweenPath.GetPath("plateformRail"), this.player.transform.position, 0.01f, out closestPoint, out this.distanceParcourue);
		dist = this.platform.distanceParcouru - this.distanceParcourue;
		if (dist < 0.0f)
		{
			this.droneBehind.SetActive(true);
		}
		else
		{
			this.droneBehind.SetActive(false);
		}
	}
}
