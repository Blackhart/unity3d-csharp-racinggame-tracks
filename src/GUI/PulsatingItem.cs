﻿using UnityEngine;
using System.Collections;

public class PulsatingItem : MonoBehaviour
{
	public UIWidget item;
	public float fadingTime;
	public float fadingTimer;

	private float lastFade = 0;
	private bool started = false;

	private void Start()
	{
		this.item.alpha = 0f;
		this.enabled = false;
		this.started = true;
	}

	private void Pulse()
	{
		if (this.started)
		{
			this.item.alpha = 1f;
			TweenAlpha.Begin(this.item.gameObject, this.fadingTime, 0f);
			this.lastFade = Time.timeSinceLevelLoad;
		}
	}

	private void Update()
	{
		if (this.lastFade + this.fadingTimer <= Time.timeSinceLevelLoad)
			this.Pulse();
	}
}
