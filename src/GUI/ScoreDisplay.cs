﻿using UnityEngine;
using System.Collections;

public class ScoreDisplay : MonoBehaviour
{
	public UISprite[] sprites;
	public UILabel scoreValue;
	public UILabel scoreMulti;
	public float currentValue;

	private ScoreMultiplicateur scoreManager;
	private Filin localPlayer;
	private bool init = false;

	private void Awake()
	{
		this.currentValue = 0;
		this.scoreValue.text = "0"; 
	}

	private void Start()
	{
		this.Init();
		this.UpdateCoeff();
	}

	public void Init()
	{
		GameObject network = GameObject.Find("Network");
		if (network)
		{
			GameLogic gameLogic = network.GetComponent<GameLogic>();
			if (gameLogic)
			{
				GameObject localPlayer = gameLogic.Player;
				if (localPlayer == null)
				{
					Debug.LogWarning(this.GetType() + ": no local player found, disabling script");
					this.enabled = false;
				}
				else
					this.localPlayer = localPlayer.GetComponent<Filin>();
			}
		}
		else
		{
			GameObject localPlayer = GameObject.Find("Player");
			if (localPlayer == null)
			{
				Debug.LogWarning(this.GetType() + ": no local player found, disabling script");
				this.enabled = false;
			}
			else
				this.localPlayer = localPlayer.GetComponent<Filin>();
		}
		GameObject platform = GameObject.Find("plateforme");
		if (platform)
			this.scoreManager = platform.GetComponent<ScoreMultiplicateur>();
		else
		{
			Debug.LogWarning(this.GetType() + ": no platform found, disabling script");
			this.enabled = false;
		}
		this.init = true;
	}

	private void UpdateCoeff()
	{
		int full = Mathf.FloorToInt(this.currentValue);
		bool max = false;

		if (this.currentValue > 0)
		{
			if (full >= this.sprites.Length)
			{
				full = this.sprites.Length - 1;
				max = true;
			}
			for (int i = 0; i < full; ++i)
				this.sprites[i].fillAmount = 1;
			this.sprites[full].fillAmount = this.currentValue - full;
			for (int i = full + 1, size = this.sprites.Length; i < size; ++i)
				this.sprites[i].fillAmount = 0;
			if (max)
				this.scoreMulti.text = "x" + this.scoreManager.m_bonus[full + 1].y;
			else
				this.scoreMulti.text = "x" + this.scoreManager.m_bonus[full].y;
		}
		else
		{
			for (int i = 0, size = this.sprites.Length; i < size; ++i)
				this.sprites[i].fillAmount = 0;
			this.scoreMulti.text = "x" + this.scoreManager.m_bonus[0].y;
		}
	}

	private void Update()
	{
		if (this.init)
		{
			this.currentValue = this.localPlayer.m_coeff;
			this.UpdateCoeff();
			this.scoreValue.text = "" + Mathf.FloorToInt(this.localPlayer.m_score);
		}
	}
}
