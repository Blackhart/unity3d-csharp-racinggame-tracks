﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ScoreRanking : MonoBehaviour
{
	public Transform[] placeholders;
	public UILabel[] labels;

	static public Dictionary<int, int> players;
	private bool init = false;
	private int localID;
	private Filin localPlayer;

	private void Start()
	{
	}

	public void Init()
	{
		GameObject network = GameObject.Find("Network");
		if (network)
		{
			players = new Dictionary<int, int>();
			GameObject[] lPlayers = GameObject.FindGameObjectsWithTag("Player");
			foreach (GameObject lObject in lPlayers)
			{
				if (players.ContainsKey(lObject.GetComponent<NetworkCharacter>().ID) == false)
					players.Add(lObject.GetComponent<NetworkCharacter>().ID, 0);
				if (lObject.GetPhotonView().isMine)
				{
					this.localID = lObject.GetComponent<NetworkCharacter>().ID;
					this.localPlayer = lObject.GetComponent<Filin>();
				}
			}
			for (int i = 0; i < lPlayers.Length; ++i)
				this.labels[i].alpha = 1f;
			for (int i = lPlayers.Length; i < this.labels.Length; ++i)
				this.labels[i].alpha = 0f;
		}
		else
			this.enabled = false;
		this.init = true;
	}

	public void Reset()
	{
		this.init = false;
	}

	private void Update()
	{
		if (this.init && GameLogic.isRaceStart)
		{
			players[this.localID] = Mathf.FloorToInt(this.localPlayer.m_score);
			players = players.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
			for (int i = 0, size = players.Count; i < size; ++i)
				this.labels[i].text = players.Keys.ElementAt(i) + "P - " + players.Values.ElementAt(i) + " Go";
		}
		else if (GameLogic.isRaceStart)
		{
			this.Init();
		}
	}

	static public void ChangeScore(int score, int ID)
	{
		if (players != null)
			players[ID] = score;
	}
}
