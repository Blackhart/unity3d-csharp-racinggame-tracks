﻿using UnityEngine;
using System.Collections;

public class SelectItem : MonoBehaviour
{
	public GameObject item;

	public void Select()
	{
		if (this.item != null)
			UICamera.selectedObject = this.item;
	}
}
