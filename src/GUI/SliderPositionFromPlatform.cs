﻿using UnityEngine;
using System.Collections;

public class SliderPositionFromPlatform : MonoBehaviour
{
	public UISlider PlatformSlider;
	public SliderPositionFromPlayer[] PlayerSliders;

	private movePlateform platform;
	private int nbPlayers;
	private bool initPlayers = false;

	private void Start()
	{
		if (this.PlatformSlider == null)
		{
			Debug.LogWarning(this.GetType() + ": no target, disabling script");
			this.enabled = false;
			return;
		}
		this.Init();
	}

	public void Init()
	{
		GameObject platform = GameObject.Find("plateforme");
		if (platform)
			this.platform = platform.GetComponent<movePlateform>();
		else
		{
			Debug.LogWarning(this.GetType() + ": no platform found, disabling script");
			this.enabled = false;
		}
		this.initPlayers = false;
	}

	public void InitPlayers()
	{
		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		this.nbPlayers = players.Length;
		for (int i = 0; i < this.nbPlayers; ++i)
		{
			this.PlayerSliders[i].gameObject.SetActive(true);
			this.PlayerSliders[i].Init(this, players[i].GetComponent<Controlleur>());
		}
		for (int i = nbPlayers; i < this.PlayerSliders.Length; ++i)
			this.PlayerSliders[i].gameObject.SetActive(false);
		this.initPlayers = true;
	}

	private void Update()
	{
		if (!this.initPlayers)
			this.InitPlayers();
		this.PlatformSlider.value = this.platform.distanceParcouru;
	}
}
