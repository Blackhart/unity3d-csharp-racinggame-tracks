﻿using UnityEngine;
using System.Collections;

public class SliderPositionFromPlayer : MonoBehaviour
{
	public UISlider slider;

	private SliderPositionFromPlatform handler;
	private Controlleur player;
	private bool init = false;
	private float distanceParcourue;

	private void Start()
	{
		if (this.slider == null)
		{
			Debug.LogWarning(this.GetType() + ": no target, disabling script");
			this.enabled = false;
			return;
		}
		this.slider.value = 0f;
	}

	public void Init(SliderPositionFromPlatform handler, Controlleur player)
	{
		this.handler = handler;
		this.player = player;
		this.slider.value = 0.001f;
		this.init = true;
	}

	private void Update()
	{
		if (this.init == false || this.player == null)
			return;
		Vector3 closestPoint = Vector3.zero;
		iTween.ClosestPointOnPath(iTweenPath.GetPath("plateformRail"), this.player.transform.position, 0.01f, out closestPoint, out this.distanceParcourue);
		this.slider.value = this.distanceParcourue;
	}
}
