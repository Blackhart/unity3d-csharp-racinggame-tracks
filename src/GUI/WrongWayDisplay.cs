﻿using UnityEngine;
using System.Collections;

public class WrongWayDisplay : MonoBehaviour
{
	public PulsatingItem wrongWayLabel;
	public float dotProductThreshold = -0.7f;
	private Transform localPlayer;
	private bool init = false;

	private void Start()
	{
		this.Init();
	}

	public void Init()
	{
		GameObject network = GameObject.Find("Network");
		if (network)
		{
			GameLogic gameLogic = network.GetComponent<GameLogic>();
			if (gameLogic)
			{
				GameObject localPlayer = gameLogic.Player;
				if (localPlayer == null)
					this.enabled = false;
				else
					this.localPlayer = localPlayer.GetComponent<Transform>();
			}
		}
		else
		{
			GameObject localPlayer = GameObject.Find("Player");
			if (localPlayer == null)
				this.enabled = false;
			else
				this.localPlayer = localPlayer.GetComponent<Transform>();
		}
		this.init = true;
	}

	private void Update()
	{
		if (this.init)
		{
			Vector3 closest = Vector3.zero;
			float percent = 0f;
			iTween.ClosestPointOnPath(iTweenPath.GetPath("plateformRail"), this.localPlayer.position, 0.01f, out closest, out percent);
			if (Vector3.Dot((this.localPlayer.forward).normalized, (iTween.PointOnPath(iTweenPath.GetPath("plateformRail"), percent + 0.001f) - closest).normalized) < this.dotProductThreshold)
			{
				if (!this.wrongWayLabel.enabled)
					this.wrongWayLabel.enabled = true;
			}
			else
				this.wrongWayLabel.enabled = false;
		}
	}
}
