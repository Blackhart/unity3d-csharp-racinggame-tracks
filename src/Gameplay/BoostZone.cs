﻿using UnityEngine;
using System.Collections;

public class BoostZone : MonoBehaviour
{
	public int boostValue = 20;

	#region members
	private Turbo _turbo;
	#endregion

	// Use this for initialization
	void Start () 
	{
		_turbo = GetComponent<Turbo>();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "ZoneBoost")
		{
			if (_turbo == null)
				return;
			_turbo.m_turboSpeed += boostValue;
		}
	}
}
