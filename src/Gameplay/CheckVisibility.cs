﻿using UnityEngine;
using System.Collections;

public class CheckVisibility : MonoBehaviour
{
	public bool visible;
	public GameObject fx;

	void Start()
	{
		this.visible = false;
		this.fx.SetActive(false);
	}

	void OnBecameVisible()
	{
		this.visible = true;
		this.fx.SetActive(true);
	}

	void OnBecameInvisible()
	{
		this.visible = false;
		this.fx.SetActive(false);
	}
}
