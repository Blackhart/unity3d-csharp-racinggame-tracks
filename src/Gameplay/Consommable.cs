﻿using UnityEngine;
using System.Collections;

public class Consommable : MonoBehaviour
{
	public float slowCoef;
	public float bonusJauge;

	#region Private members
	private Jauge _jauge;
	private Controlleur _hoverboard;
	private AnimationController animController;
	private FXManager fxManager;
	#endregion

	// Use this for initialization
	void Start()
	{
		_jauge = GetComponent<Jauge>();
		_hoverboard = GetComponent<Controlleur>();
		this.animController = this.GetComponent<AnimationController>();
		this.fxManager = this.GetComponent<FXManager>();
	}

	void OnTriggerEnter(Collider pCol)
	{
		if (pCol.tag == "Boost" && pCol.gameObject.GetComponent<Destructible>().destroyed == false)
		{
			this.fxManager.grabCollectiblePart1.particleSystem.Play();
			this.fxManager.grabCollectiblePart2.particleSystem.Play();
			this._jauge.m_jauge += bonusJauge;
			pCol.gameObject.GetComponent<Destructible>().destroyed = true;
			Destroy(pCol.gameObject);
		}
		else if (pCol.tag == "Obstacle")
		{
			this.animController.hit = true;
			_hoverboard.currentSpeed -= (_hoverboard.currentSpeed * slowCoef);
			// call it for playing anim on destruction
			//pCol.gameObject.GetComponent<Destructible>().PlayAnim();
			// call it for playing particules on destruction
			Destroy(pCol.gameObject);
		}
	}
}
