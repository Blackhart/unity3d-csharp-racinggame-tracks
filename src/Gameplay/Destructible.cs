﻿using UnityEngine;
using System.Collections;

public class Destructible : MonoBehaviour
{
	public GameObject mech_FX;
	public Animation anim;
	public bool destroyed = false;
	private bool animLaunched = false;
	private bool isQuitting = false;

	void Update()
	{
		if (this.animLaunched == true)
		{
			if (this.anim.isPlaying == false)
			{
				Destroy(this.gameObject);
			}
		}
	}

	void OnApplicationQuit()
	{
		this.isQuitting = true;
	}

	void OnDestroy()
	{
		if (this.animLaunched == false && this.isQuitting == false && Application.isLoadingLevel == false)
		{
			Instantiate(this.mech_FX, this.transform.position, Quaternion.identity);
		}
	}

	public void PlayAnim()
	{
		this.anim.Play();
		this.animLaunched = true;
	}
}
