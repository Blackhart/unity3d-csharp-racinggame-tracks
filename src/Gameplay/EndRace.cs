﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EndRace : MonoBehaviour
{
	private Vector3 PositionStart;
	private Quaternion RotationStart;
	private RandomMatchmaker matchMaker;
	public static bool inResult = false;
	private GameObject collectibles;

	public GameObject Obj;
	public bool Network;
	public Chrono chrono;
	public GameObject panelResult;
	public UISprite background;
	public UILabel[] scoresLabel;
	public UILabel[] namesLabel;
	public UILabel chronoLabel;
	public GameObject pausePanel;
	public UISprite pauseBackground;
	private bool _pause = false;

	// Use this for initialization
	void Start()
	{
		if (this.pausePanel != null)
		{
			this.pausePanel.SetActive(false);
			this.pauseBackground.width = Screen.width;
			this.pauseBackground.height = Screen.height;
			this.pauseBackground.leftAnchor.absolute = 0;
			this.pauseBackground.rightAnchor.absolute = 0;
			this.pauseBackground.bottomAnchor.absolute = 0;
			this.pauseBackground.topAnchor.absolute = 0;
		}
		this.panelResult.SetActive(false);
		this.background.width = Screen.width;
		this.background.height = Screen.height;
		this.background.leftAnchor.absolute = 0;
		this.background.rightAnchor.absolute = 0;
		this.background.bottomAnchor.absolute = 0;
		this.background.topAnchor.absolute = 0;
		if (Obj == null)
			return;
		PositionStart = Obj.transform.position;
		RotationStart = Obj.transform.rotation;
		if (Network)
			this.matchMaker = GameObject.Find("Network").GetComponent<RandomMatchmaker>();
		this.collectibles = GameObject.Find("Collectibles");
	}

	// Update is called once per frame
	void Update()
	{
		if (!Network)
		{
			if (Input.GetKeyDown(KeyCode.R))
				this.RestartGameNoNetWork();
			if (Input.GetKey(KeyCode.Escape))
			{
				this.showPauseUI();
			}
		}
		else
		{
			if (Input.GetKey(KeyCode.Escape))
			{
				Camera.main.GetComponent<CameraController>().enabled = false;
				//GameObject.Find("GUI").GetComponent<GUI_Parcours>().enabled = false;
				//GameObject.Find("GUI").GetComponent<GUI_score>().enabled = false;
				//GameObject.Find("GUI").GetComponent<GUI_classement>().enabled = false;
				//GameObject.Find("score").GetComponent<GUIText>().enabled = false;
				//GameObject.Find("Jauge").GetComponent<GUIText>().enabled = false;
				//GameObject.Find("coeffMult").GetComponent<GUIText>().enabled = false;
				GameObject.Find("Network").GetComponent<Lan_broadcast>().__udp_port++;
				this.RestartGameNetWork();
				this.matchMaker.BackToMenu();
			}
		}
	}

	void DrawResults()
	{
		int i = 0;

		GameObject.Find("UI Root - Ingame").transform.GetChild(0).gameObject.SetActive(false);
		this.panelResult.SetActive(true);
		this.panelResult.GetComponent<SelectItem>().Select();
		if (this.Network == false)
		{
			this.scoresLabel[0].text = "Player 1 " + this.chronoLabel.text;
		}
		else if (ScoreRanking.players != null)
		{
			ScoreRanking.players = ScoreRanking.players.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
			foreach (KeyValuePair<int, int> lPair in ScoreRanking.players)
			{
				this.namesLabel[i].text = lPair.Key + "P";
				this.scoresLabel[i].text = lPair.Value.ToString() + " Go";
				i++;
			}
		}
		inResult = true;
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Plateforme") == true && Network == true)
		{
			this.DrawResults();
			//GameObject.Find("GUI").GetComponent<GUI_Parcours>().enabled = false;
			//GameObject.Find("GUI").GetComponent<GUI_classement>().enabled = false;
			GameLogic.isRaceStart = false;
			GameLogic.isInPreRace = false;
			GameLogic.countdown = 3.0f;
			Camera.main.GetComponent<CameraController>().enabled = false;
		}
		else if (col.gameObject.CompareTag("Player") == true && Network == false)
		{
			if (this.Obj == null)
				return;
			this.DrawResults();
			//this.RestartGameNoNetWork();
		}
	}

	public void CallEndRace()
	{
		this.panelResult.SetActive(false);
		inResult = false;
		GameLogic.SendEndRace(RandomMatchmaker.playersList, PositionStart, RotationStart);
	}

	public void RestartGameNoNetWork()
	{
		inResult = false;
		this.panelResult.SetActive(false);
		Obj.transform.position = PositionStart;
		Obj.transform.rotation = RotationStart;
		Obj.GetComponent<Controlleur>().currentSpeed = 0;
		Obj.GetComponent<Controlleur>().boostSpeed = 0;
		Obj.GetComponent<Moteur>().source.Play();
		Obj.GetComponent<Jauge>().m_jauge = Obj.GetComponent<Jauge>().m_maxJaugeValue;
		GameObject.Destroy(this.collectibles);
		GameObject.Find("UI Root - Ingame").transform.GetChild(0).gameObject.SetActive(true);
		this.collectibles = GameObject.Instantiate(Resources.Load("Collectibles")) as GameObject;
		GameObject.Find("plateforme").GetComponent<movePlateform>().distanceParcouru = 0.0f;
		this.chrono.ResetTimer();
	}

	public void RestartGameNetWork()
	{
		Obj.transform.position = PositionStart;
		Obj.transform.rotation = RotationStart;
		Camera.main.fieldOfView = Obj.GetComponent<CameraController>().mDefaultFOV;
		GameLogic.SendEndRace(RandomMatchmaker.playersList, PositionStart, RotationStart);
	}

	private void pause(bool value = true)
	{
		this._pause = value;

		Obj.GetComponent<Controlleur>().enabled = !this._pause;
		Obj.GetComponent<Turbo>().enabled = !this._pause;
		Obj.GetComponent<stabilityBoard>().enabled = !this._pause;
		Obj.GetComponent<Filin>().enabled = !this._pause;
		this.chrono.started = !value;
		if (this._pause)
			Obj.rigidbody.Sleep();
		else
			Obj.rigidbody.WakeUp();
	}

	private void returnMenu()
	{
		Application.LoadLevel(0);
	}

	private void quitGame()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void resumeGame()
	{
		this.pausePanel.SetActive(false);
		this.pause(false);
	}

	public void quitRace()
	{
		returnMenu();
	}

	private void showPauseUI()
	{
		this.pausePanel.SetActive(true);
		this.pause();
		this.pausePanel.GetComponent<SelectItem>().Select();
	}
}
