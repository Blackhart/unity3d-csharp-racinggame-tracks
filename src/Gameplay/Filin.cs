﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#region Enum
public enum eFilinState
{
	_ACTIVE_ = 0,
	_DISACTIVE_,
	_CUT_
}
#endregion

public class Filin : Photon.MonoBehaviour
{
	#region public members
	public GameObject m_target; // plateforme
	public float m_maxDist; // Distance max à laquelle le joueur peux s'accrocher à la plateforme (en unité)
	public float m_scoreAdd; // nombre de points à ajouter au joueur par secondes
	public float m_score // score du joueur;
	{
		private set
		{
			_score = value;
		}
		get
		{
			return _score;
		}
	}
	public float m_coeff
	{
		get
		{
			return _scoreMultScript.getProgress(_time);
		}
	}
	public GUIText m_indicBonus; // text bonus
	public eFilinState _filinState;
	public string eventName;
	public FmodEventAsset asset;
	public FmodEventAudioSource source;
	public float WidthFilin = 0.01f;
	#endregion

	#region private members
	private ScoreMultiplicateur _scoreMultScript;
	private PlateformData _plateformData;
	private RaycastHit _rayCastData;
	private Transform _filinPos;
	private float _score;
	private float _time;
	public LineRenderer _lineRender;
	private AnimationController animationController;
	#endregion

	#region Init

	void Awake()
	{
		if (Moteur.CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
		else
		{
			this.source.playOnAwake = false;
		}
	}

    public void Start()
    {
		if (this.GetComponent<Controlleur>().noNetwork)
		{
			Init();
		}
    }

	public void Init()
	{
		_filinState = eFilinState._DISACTIVE_;
		/*_lineRender = gameObject.AddComponent<LineRenderer>();
		_lineRender.material.color = Color.red;*/
		_lineRender.SetWidth(0.01f, 0.01f);
		_lineRender.SetVertexCount(2);
		_lineRender.enabled = false;
		_filinPos = transform.FindChild("FilinPos");
		_score = 0.0f;
		_time = 0.0f;
        _scoreMultScript = m_target.GetComponent<ScoreMultiplicateur>();
        _plateformData = m_target.GetComponent<PlateformData>();
		updateTextBonus();
		this.animationController = this.GetComponent<AnimationController>();
	}
	#endregion

	#region Update
	void LateUpdate()
	{
		// Error
		if (!_filinPos || !_scoreMultScript || !_plateformData)
			return;
		if (photonView.isMine && GameLogic.isRaceStart || this.GetComponent<Controlleur>().noNetwork)
			getState();
		if (_filinState == eFilinState._ACTIVE_)
			activate();
		else if (_filinState == eFilinState._DISACTIVE_)
			disactive();
		else if (_filinState == eFilinState._CUT_)
			cut();
		if (photonView.isMine && GameLogic.isRaceStart || this.GetComponent<Controlleur>().noNetwork)
			updateTextBonus();
	}
	#endregion

	#region GameLogic
	void activate()
	{
		_lineRender.enabled = true;
		_lineRender.SetWidth(WidthFilin, WidthFilin);
		_lineRender.SetPosition(0, _filinPos.position);
		_lineRender.SetPosition(1, m_target.transform.position);
		_plateformData.m_Player = gameObject;
		if (photonView.isMine || this.GetComponent<Controlleur>().noNetwork)
		{
			this.source.Play();
			addScore();
		}
	}

	void disactive()
	{
		if (this.source.getStatus() != "Stopped")
		{
			this.source.Stop();
		}
		_lineRender.enabled = false;
		if (_plateformData.m_Player == gameObject)
			_plateformData.m_Player = null;
		if (photonView.isMine || this.GetComponent<Controlleur>().noNetwork)
			_time = _scoreMultScript.getLastPalier(_time);
	}

	void cut()
	{
		if (this.source.getStatus() != "Stopped")
		{
			this.source.Stop();
		}
		_lineRender.enabled = false;
		if (_plateformData.m_Player == gameObject)
			_plateformData.m_Player = null;
		if (photonView.isMine || this.GetComponent<Controlleur>().noNetwork)
			_time = 0.0f;
	}

	void getState()
	{
		bool lStateMan = Input.GetButton("Filin");

		if (!lStateMan)
			_filinState = eFilinState._DISACTIVE_;
		else
		{
			if (_filinState != eFilinState._CUT_ && (!_plateformData.m_Player || _plateformData.m_Player == gameObject) &&
				LaunchRay(_filinPos.position, m_target.transform.position - _filinPos.position))
			{
				_filinState = eFilinState._ACTIVE_;
				this.animationController.grap = true;
			}
			else
				_filinState = eFilinState._CUT_;
		}
	}

    RaycastHit[] SortTargetsByDistance(RaycastHit[] hit)
	{
		int count = hit.Length;
		RaycastHit[] ret = hit;
        Dictionary<float, RaycastHit> lDico = new Dictionary<float, RaycastHit>();

        for (int i = 0; i < count; i++)
        {
            float lKey = hit[i].distance;
            while (lDico.ContainsKey(lKey))
                lKey += 0.01f;
            lDico.Add(lKey, hit[i]);
        }
        lDico = lDico.OrderBy(kvp => kvp.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        int index = 0;
        foreach (KeyValuePair<float, RaycastHit> elem in lDico)
        {
            ret[index] = elem.Value;
            index++;
        }
        return ret;
	}

	bool LaunchRay(Vector3 pStart, Vector3 pEnd)
	{
		int lLayer = 1 << 11;
		int count;

		lLayer = ~lLayer;

		RaycastHit[] hit = Physics.RaycastAll(pStart, pEnd, /*out _rayCastData,*/ m_maxDist/*, lLayer*/);
        hit = this.SortTargetsByDistance(hit);
		count = hit.Length;
		for (int i = 0; i < count; i++)
		{
			if (hit[i].collider.gameObject == this.gameObject || hit[i].collider.gameObject == m_target)
			{
				if (hit[i].collider.gameObject == m_target)
				{
					return true;
				}
			}
			else
				return false;
		}
		return false;
	}

	void addScore()
	{
		_time += Time.deltaTime;
		m_score += m_scoreAdd * Time.deltaTime * _scoreMultScript.getCoeff(_time);
		if (photonView.isMine)
		{
			GameLogic.SendScore((int)m_score, this.GetComponent<NetworkCharacter>().ID);
		}
	}

	public GameObject getPlayer()
	{
		if (_plateformData == null)
			return null;
		return _plateformData.m_Player;
	}

	#endregion

	#region GUI
	void updateTextBonus()
	{
		float lCoeff;

		lCoeff = _scoreMultScript.getCoeff(_time);
		if (m_indicBonus != null)
			m_indicBonus.text = "x" + lCoeff;
	}
	#endregion

	#region Reseau
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(this._filinState);
		}
		else
		{
			// Network player, receive data
			this._filinState = (eFilinState)stream.ReceiveNext();
		}
	}
	#endregion

	#region Tools

	private void OnDrawGizmos()
	{
		Color color = Gizmos.color;
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.FindChild("FilinPos").position, this.m_maxDist);
		Gizmos.color = color;
	}
	#endregion
}