﻿using UnityEngine;
using System.Collections;

public class Jauge : MonoBehaviour
{

	#region Private members
	public float _jauge;
	public float m_jauge
	{
		get
		{
			return _jauge;
		}
		set
		{
			_jauge = Mathf.Clamp(value, 0.0f, m_maxJaugeValue);
			if (this.m_indicJauge)
			{
				m_indicJauge.text = "Jauge: " + (int)_jauge;
			}
		}
	}
	private Turbo turbo;
	#endregion

	#region Public members
	public float m_regenJauge; // Regen Jauge/s
	public float m_maxJaugeValue = 70.0f; //Jauge maximum value
	public float m_maxRegenValue = 30.0f; //Jauge will regen until this valeu is reached
	public GUIText m_indicJauge; // Text Jauge lvl
	public FmodEventAudioSource source;
	public string eventName;
	public FmodEventAsset asset;
	#endregion

	void Awake()
	{
		if (Moteur.CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
		else
		{
			this.source.playOnAwake = false;
		}
	}

	// Use this for initialization
	void Start()
	{
		m_jauge = m_maxJaugeValue;
		this.turbo = this.GetComponent<Turbo>();
	}

	// Update is called once per frame
	void Update()
	{
		RegenJauge();
	}

	// Regen/seconde Jauge
	private void RegenJauge()
	{
		if (m_jauge >= m_maxRegenValue)
			return;
		m_jauge = Mathf.Clamp(m_jauge + m_regenJauge * Time.deltaTime, 0.0f, m_maxRegenValue);
		if (this.turbo.m_turbo == false)
		{
			this.source.Play();
		}
	}
}
