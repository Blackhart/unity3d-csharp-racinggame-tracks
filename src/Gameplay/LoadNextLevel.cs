﻿using UnityEngine;
using System.Collections;

public class LoadNextLevel : MonoBehaviour
{
	public GameObject murToLoad;
	public GameObject roadToLoad;
	public GameObject murToUnload;
	public GameObject roadToUnload;
	public bool firstPart;
	public bool network;

	private int nbPlayersInZone = 0;

	void Start()
	{
		if (this.firstPart == true)
		{
			if (this.network == false)
			{
				this.nbPlayersInZone = 1;
			}
		}
	}

	public void Init()
	{
		this.nbPlayersInZone = RandomMatchmaker.playersList.Count;
	}

	void OnEnable()
	{
		this.nbPlayersInZone++;
	}

	void VerifNbPlayersInZone()
	{
		if (this.nbPlayersInZone == 0)
		{
			this.murToLoad.SetActive(true);
			this.roadToLoad.SetActive(true);
			this.murToUnload.SetActive(false);
			this.roadToUnload.SetActive(false);
		}
		else
		{
			if (this.murToUnload.activeInHierarchy == false)
			{
				this.murToUnload.SetActive(true);
				this.roadToUnload.SetActive(true);
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player") == true && !other.isTrigger)
		{
			float angle = Vector3.Angle(other.transform.forward, this.transform.forward);

			if (angle > 0.0f && angle <= 90.0f)
			{
				this.nbPlayersInZone--;
				this.VerifNbPlayersInZone();
			}
			else
			{
				this.nbPlayersInZone++;
				this.VerifNbPlayersInZone();
			}
		}
	}
}
