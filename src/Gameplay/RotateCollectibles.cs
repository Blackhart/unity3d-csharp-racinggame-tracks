﻿using UnityEngine;
using System.Collections;

public class RotateCollectibles : MonoBehaviour
{
	public float speed = 40.0f;
	public CheckVisibility visibility;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (this.visibility.visible == true)
		{
			this.gameObject.transform.Rotate(Vector3.up * (this.speed * Time.deltaTime));
		}
	}
}
