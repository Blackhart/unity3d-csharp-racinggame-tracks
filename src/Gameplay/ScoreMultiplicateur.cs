﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreMultiplicateur : MonoBehaviour {
	
	#region Public Members
	public	List<Vector2>	m_bonus = new List<Vector2>(); // Score multiplicateurs [X: temps en secondes; Y: coeff]
	#endregion

	public float	getProgress(float pTime)
	{
		float value = 0f;

		for (int lIndex = 1; lIndex < m_bonus.Count; lIndex++)
		{
			if (pTime > m_bonus[lIndex].x)
				value++;
			else
			{
				float percent = ((pTime - m_bonus[lIndex - 1].x) / (m_bonus[lIndex].x - m_bonus[lIndex - 1].x));
				if (percent <= 0)
					break;
				value += percent;
			}
		}
		return value;
	}

	public float getCoeff(float pTime)
	{
		int lIndex = 0;
		for ( ; lIndex < m_bonus.Count; lIndex++)
		{
			if (m_bonus[lIndex].x > pTime)
			{
				if (lIndex - 1 < 0)
					return 0;
				else
					return m_bonus[lIndex - 1].y;
			}
		}
		return m_bonus[lIndex - 1].y;
	}
	
	public float	getLastPalier(float pTime)
	{
		int lIndex = 0;
		for ( ; lIndex < m_bonus.Count; lIndex++)
		{
			if (m_bonus[lIndex].x > pTime)
			{
				if (lIndex - 1 < 0)
					return 0;
				else
					return m_bonus[lIndex - 1].x;
			}
		}
		return m_bonus[lIndex - 1].x;
	}
}
