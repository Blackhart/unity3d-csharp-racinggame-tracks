using UnityEngine;
using System.Collections.Generic;

public class GameLogic : Photon.MonoBehaviour
{
	public static int playerWhoIsIt;
	public static int nbPlayers;
	public static bool isLaunch;
	public static bool isRaceStart = false;
	public static bool isInPreRace = false;
	public static float countdown = 3.0f;
	public static List<GameObject> spawnList;
	public GameObject background;

	private LoadNextLevel loading;
	private GameObject collectibles;
	private static PhotonView ScenePhotonView;
	private List<Player> playerList;
	private GameObject player;
	private AudioClipManager soundManager;
	private FadingText countdownText;
	public GameObject Player
	{
		private set
		{
		}
		get
		{
			return player;
		}
	}

	void Start()
	{
		this.soundManager = Camera.main.GetComponent<AudioClipManager>();
		this.loading = GameObject.Find("Trigger_LVL02").GetComponent<LoadNextLevel>();
		ScenePhotonView = this.GetComponent<PhotonView>();
		this.playerList = new List<Player>();
		spawnList = new List<GameObject>();
		spawnList.Add(GameObject.Find("PointSpawn1"));
		spawnList.Add(GameObject.Find("PointSpawn2"));
		spawnList.Add(GameObject.Find("PointSpawn3"));
		spawnList.Add(GameObject.Find("PointSpawn4"));
		isLaunch = false;
	}

	void Update()
	{
		if (isInPreRace)
		{
			if (this.Countdown())
			{
				SendStart();
				isInPreRace = false;
			}
		}
	}

	void OnJoinedRoom()
	{
		if (PhotonNetwork.playerList.Length == 1)
		{
			Player hostPlayer = new Player(PhotonNetwork.player.ID, 1);

			playerWhoIsIt = PhotonNetwork.player.ID;
			RandomMatchmaker.playersList.Add(hostPlayer);
			nbPlayers = 1;
		}
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		Debug.Log("Player Disconnected " + player.ID);

		Player toDisconnect = GetPlayerByID(player.ID);
		for (int i = 0; i < RandomMatchmaker.playersList.Count; ++i)
		{
			if (toDisconnect.ID < RandomMatchmaker.playersList[i].ID)
			{
				--RandomMatchmaker.playersList[i].ID;
			}
		}
		if (toDisconnect == null)
		{
			Debug.LogError("Player " + player.ID + " not found!");
		}

		if (PhotonNetwork.isMasterClient)
		{
			if (player.ID == playerWhoIsIt)
			{
				// if the player who left was "it", the "master" is the new "it"
				TagPlayer(PhotonNetwork.player.ID);
			}
		}
		RandomMatchmaker.playersList.Remove(toDisconnect);
	}

	void OnPhotonPlayerConnected(PhotonPlayer player)
	{

		// when new players join, we send "who's it" to let them know
		// only one player will do this: the "master"

		if (PhotonNetwork.isMasterClient)
		{
			if (GameLogic.isLaunch)
			{
				ScenePhotonView.RPC("GameAlreadyLaunch", player);
			}
			else
			{
				Player newPlayer = new Player(player.ID, RandomMatchmaker.playersList.Count + 1);
				Debug.Log("Player Connected " + player.ID);
				bool[] states = new bool[RandomMatchmaker.playersList.Count + 1];
				int[] PhotonID = new int[RandomMatchmaker.playersList.Count + 1];
				int[] ID = new int[RandomMatchmaker.playersList.Count + 1];
				string[] models = new string[RandomMatchmaker.playersList.Count + 1];

				TagPlayer(playerWhoIsIt);
				RandomMatchmaker.playersList.Add(newPlayer);
				for (int i = 0; i < RandomMatchmaker.playersList.Count; ++i)
				{
					states[i] = RandomMatchmaker.playersList[i].isReady;
					PhotonID[i] = RandomMatchmaker.playersList[i].PhotonID;
					ID[i] = RandomMatchmaker.playersList[i].ID;
					models[i] = RandomMatchmaker.playersList[i].characModel;
				}
				ScenePhotonView.RPC("SetList", PhotonTargets.All, RandomMatchmaker.playersList.Count, states, PhotonID, ID, models);
			}
		}
	}

	private bool Countdown()
	{
		GameLogic.countdown -= Time.deltaTime;
		if (GameLogic.countdown < 0.0f)
		{
			if (this.soundManager != null)
				this.soundManager.PlayOnce("Go");
			if (this.countdownText != null)
				ScenePhotonView.RPC("SetCountdown", PhotonTargets.All, "GO !", 3.0f);
			return true;
		}
		if ((GameLogic.countdown + Time.deltaTime) - Mathf.FloorToInt(GameLogic.countdown + Time.deltaTime) < Time.deltaTime)
		{
			if (this.soundManager != null)
				this.soundManager.PlayOnce("Ready");
			if (this.countdownText != null)
				ScenePhotonView.RPC("SetCountdown", PhotonTargets.All, "" + Mathf.FloorToInt(GameLogic.countdown + Time.deltaTime), 0.75f);
		}
		return false;
	}

	public static Player GetPlayerByID(int PhotonID)
	{
		for (int i = 0; i < RandomMatchmaker.playersList.Count; ++i)
		{
			if (RandomMatchmaker.playersList[i].PhotonID == PhotonID)
			{
				return RandomMatchmaker.playersList[i];
			}
		}
		return null;
	}

	public static void InitNetworkPlayer(GameObject player, int ID)
	{
		string str = "PointSpawn" + ID;
		GameObject spawn = GameObject.Find(str);
		Transform tmp = player.transform.FindChild("Hoverboard").parent;
		Destroy(player.transform.FindChild("Hoverboard").gameObject);
		GameObject obj = GameObject.Instantiate(Resources.Load(RandomMatchmaker.playersList[ID - 1].characModel + "/Hoverboard"), tmp.position, spawn.transform.rotation) as GameObject;
		obj.transform.parent = tmp;
		Vector3 vec = new Vector3(0.0f, spawn.transform.eulerAngles.y, 0.0f);
		obj.transform.localEulerAngles = vec;
		player.GetComponent<Controlleur>().body = obj;
		player.GetComponent<Controlleur>().noNetwork = false;
		player.GetComponent<Controlleur>().rail = null;
		player.GetComponent<Controlleur>().Init();
		player.GetComponent<Filin>().m_target = GameObject.Find("plateforme");
		//player.GetComponent<Filin>().m_indicBonus = GameObject.Find("coeffMult").guiText;
		player.GetComponent<Filin>().Init();
		//player.GetComponent<Jauge>().m_indicJauge = GameObject.Find("Jauge").guiText;
		player.GetComponent<AnimationController>().platform = GameObject.Find("plateforme").transform;
		//player.GetComponent<AnimationController>().animator = obj.transform.FindChild(RandomMatchmaker.playersList[ID - 1].characModel).gameObject.GetComponent<Animator>();
		player.GetComponent<AnimationController>().anim = obj.transform.FindChild(RandomMatchmaker.playersList[ID - 1].characModel).gameObject.GetComponent<Animation>();
	}

	public static void SetPlayer(GameObject player, int index)
	{
		player.GetComponent<NetworkCharacter>().ID = RandomMatchmaker.playersList[index].ID;
		if (!player.GetPhotonView().isMine)
		{
			GameLogic.InitNetworkPlayer(player, player.GetComponent<NetworkCharacter>().ID);
		}
	}

	public static void StartCountdown()
	{
		if (PhotonNetwork.isMasterClient)
		{
			isInPreRace = true;
		}
	}

	public static void SendChangeState(int index, bool state)
	{
		ScenePhotonView.RPC("ChangeStatePlayer", PhotonTargets.All, index, state);
	}

	public static void SendChangePerso(int index, string perso)
	{
		ScenePhotonView.RPC("ChangePerso", PhotonTargets.All, index, perso);
	}

	public static void SendLaunchGame(List<Player> playerList)
	{
		for (int i = 0; i < playerList.Count; ++i)
		{
			ScenePhotonView.RPC("LaunchGame", PhotonNetwork.playerList[i], i + 1);
		}
	}

	public static void TagPlayer(int playerID)
	{
		ScenePhotonView.RPC("TaggedPlayer", PhotonTargets.All, playerID);
	}

	public static void SendStart()
	{
		ScenePhotonView.RPC("StartRace", PhotonTargets.All);
	}

	// tcheat RPC restart race R
	public static void SendRestart()
	{
		ScenePhotonView.RPC("Restart", PhotonTargets.All);
	}

	public static void SendEndRace(List<Player> playerList, Vector3 pos, Quaternion rot)
	{
		for (int i = 0; i < playerList.Count; ++i)
		{
			ScenePhotonView.RPC("EndRace", PhotonTargets.All, i, pos, rot);
		}
	}

	public static void SendScore(int score, int ID)
	{
		ScenePhotonView.RPC("ChangeScore", PhotonTargets.All, score, ID);
	}

	public static void SendLoadingScreen()
	{
		ScenePhotonView.RPC("LoadScreen", PhotonTargets.All);
	}

	[RPC]
	void SetCountdown(string text, float duration)
	{
		this.countdownText.Set(text, duration); 
	}

	[RPC]
	void TaggedPlayer(int playerID)
	{
		playerWhoIsIt = playerID;
		//Debug.Log("tag: " + playerID);
	}

	[RPC]
	void GameAlreadyLaunch()
	{
		RandomMatchmaker.playersList.Clear();
		PhotonNetwork.Disconnect();
		RandomMatchmaker.isInLobby = true;
		Debug.Log("Game already launch");
	}

	[RPC]
	void SetList(int nbPlayerConnected, bool[] states, int[] PhotonID, int[] ID, string[] models)
	{
		nbPlayers = nbPlayerConnected;
		this.playerList.Clear();
		for (int i = 0; i < nbPlayers; ++i)
		{
			Player newPlayer = new Player(PhotonID[i], ID[i]);
			this.playerList.Add(newPlayer);
			this.playerList[i].isReady = states[i];
			this.playerList[i].characModel = models[i];
		}
		RandomMatchmaker.playersList = this.playerList;
	}

	[RPC]
	void ChangeStatePlayer(int index, bool state)
	{
		if (this.playerList.Count > 0)
		{
			this.playerList[index].isReady = state;
			RandomMatchmaker.playersList[index].isReady = state;
		}
	}

	[RPC]
	void ChangePerso(int index, string perso)
	{
		if (this.playerList.Count > 0)
		{
			this.playerList[index].characModel = perso;
			RandomMatchmaker.playersList[index].characModel = perso;
		}
	}

	[RPC]
	void StartRace()
	{
		isRaceStart = true;
		GameObject.Find("plateforme").GetComponent<movePlateform>().isLaunch = true;
		GameObject.Find("plateforme").GetComponent<ElectricDischarge>().Init();
		//GameObject.Find("GUI").GetComponent<GUI_classement>().Init();
		//Debug.Log("receive start");
	}

	[RPC]
	void LoadScreen()
	{
		RandomMatchmaker.loadingMulti.gameObject.SetActive(true);
	}

	[RPC]
	void LaunchGame(int ID)
	{
		if (isLaunch == false)
		{
			RandomMatchmaker.loadingMulti.ResetTimer();
			Camera.main.light.enabled = false;
			RandomMatchmaker.loadingMulti.gameObject.SetActive(false);
			RandomMatchmaker.accueil.SetActive(false);
			RandomMatchmaker.room.SetActive(false);
			RandomMatchmaker.lobby.SetActive(false);
			RandomMatchmaker.changeCharacterSolo.SetActive(false);
			string str = "PointSpawn" + ID;
			GameObject spawn = GameObject.Find(str);
			this.player = PhotonNetwork.Instantiate("Player", spawn.transform.position, Quaternion.identity, 0);
			GameLogic.InitNetworkPlayer(this.player, ID);
			//GameObject.Find("GUI").GetComponent<GUI_Parcours>().enabled = true;
			//GameObject.Find("GUI").GetComponent<GUI_score>().init();
			//GameObject.Find("GUI").GetComponent<GUI_score>().enabled = true;
			//GameObject.Find("GUI").GetComponent<GUI_classement>().enabled = true;
			//GameObject.Find("score").GetComponent<GUIText>().enabled = true;
			//GameObject.Find("Jauge").GetComponent<GUIText>().enabled = true;
			//GameObject.Find("coeffMult").GetComponent<GUIText>().enabled = true;
			GameObject.Find("UI Root - Ingame").transform.GetChild(0).gameObject.SetActive(true);
			this.countdownText = GameObject.Find("InfoLabel").GetComponent<FadingText>();
			this.player.GetComponent<AnimationController>().platformInfos = GameObject.Find("Panel - Info").GetComponent<PlateformeInfos>();
			GameObject.Find("PlatformSlider").GetComponent<SliderPositionFromPlatform>().Init();
			GameObject.Find("BoostDisplay").GetComponent<BoostDisplay>().Init();
			GameObject.Find("ScoreDisplay").GetComponent<ScoreDisplay>().Init();
			GameObject.Find("ScoreRanking").GetComponent<ScoreRanking>().Reset();
			GameObject.Find("WrongWayLabel").GetComponent<WrongWayDisplay>().Init();
			this.collectibles = GameObject.Instantiate(Resources.Load("Collectibles")) as GameObject;
			Camera.main.GetComponent<CameraController>().mCar = this.player;
			Camera.main.GetComponent<CameraController>().mCamTarget = this.player.transform.FindChild("CamTarget");
			Camera.main.GetComponent<CameraController>().mLookTarget = this.player.transform.FindChild("LookTarget");
			Camera.main.GetComponent<CameraController>().enabled = true;
			this.background.SetActive(false);
			this.loading.Init();
			isLaunch = true;
		}
	}

	// tcheat code R for restart race
	[RPC]
	void Restart()
	{
		GameLogic.countdown = 3.0f;
		PhotonNetwork.Destroy(this.player);

		this.player = PhotonNetwork.Instantiate("Player", GameObject.Find("PointSpawn1").transform.position, Quaternion.identity, 0);
		GameLogic.InitNetworkPlayer(this.player, 1);

		//GameObject.Find("GUI").GetComponent<GUI_Parcours>().enabled = true;
		//GameObject.Find("GUI").GetComponent<GUI_score>().init();
		//GameObject.Find("GUI").GetComponent<GUI_score>().enabled = true;
		//GameObject.Find("score").GetComponent<GUIText>().enabled = true;
		//GameObject.Find("Jauge").GetComponent<GUIText>().enabled = true;
		//GameObject.Find("coeffMult").GetComponent<GUIText>().enabled = true;
		GameObject.Find("PlatformSlider").GetComponent<SliderPositionFromPlatform>().Init();
		GameObject.Find("BoostDisplay").GetComponent<BoostDisplay>().Init();
		GameObject.Find("ScoreDisplay").GetComponent<ScoreDisplay>().Init();
		GameObject.Find("ScoreRanking").GetComponent<ScoreRanking>().Reset();
		GameObject.Find("WrongWayLabel").GetComponent<WrongWayDisplay>().Init();
		Camera.main.GetComponent<CameraController>().mCar = this.player;

		StartCountdown();
	}

	[RPC]
	void EndRace(int ID, Vector3 pos, Quaternion rot)
	{
		if (this.player)
		{
			PhotonNetwork.Destroy(this.player);
		}
		this.player = null;
		GameObject.Find("EndRace").GetComponent<EndRace>().panelResult.SetActive(false);
		GameObject.Find("Network").GetComponent<RandomMatchmaker>().ClickOnBackResults();
		GameObject.Destroy(this.collectibles);
		//GameObject.Find("GUI").GetComponent<GUI_Parcours>().enabled = false;
		//GameObject.Find("GUI").GetComponent<GUI_score>().enabled = false;
		//GameObject.Find("GUI").GetComponent<GUI_classement>().enabled = false;
		//GameObject.Find("score").GetComponent<GUIText>().enabled = false;
		//GameObject.Find("Jauge").GetComponent<GUIText>().enabled = false;
		//GameObject.Find("coeffMult").GetComponent<GUIText>().enabled = false;
		GameObject.Find("plateforme").GetComponent<movePlateform>().isLaunch = false;
		GameLogic.countdown = 3.0f;
		GameObject.Find("plateforme").GetComponent<movePlateform>().distanceParcouru = 0.0f;
		//Vector3 lPoint = iTween.PointOnPath(iTweenPath.GetPath("plateformRail"), 0.0f);
		//GameObject.Find("plateforme").transform.LookAt(lPoint);
		//GameObject.Find("plateforme").transform.position = lPoint;

		Camera.main.transform.position = pos;
		Camera.main.transform.rotation = rot;
		Camera.main.GetComponent<Camera>().fieldOfView = 60f;
		isLaunch = false;
		isRaceStart = false;
		RandomMatchmaker.isNeedSetOtherPlayers = true;
		RandomMatchmaker.playersList[ID].isReady = false;
		Camera.main.GetComponent<CameraController>().enabled = false;
		GameObject.Find("UI Root - Ingame").transform.GetChild(0).gameObject.SetActive(false);
		this.background.SetActive(true);
		Camera.main.light.enabled = true;
	}

	[RPC]
	void ChangeScore(int score, int ID)
	{
		ScoreRanking.ChangeScore(score, ID);
	}
}