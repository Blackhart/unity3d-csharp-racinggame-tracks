using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

public class Lan_broadcast : MonoBehaviour
{
	private enum e_state { ACTIVE, INACTIVE };
	public int __udp_port = 22124;
	private UdpClient __objUDPClient;
	private Queue<string> ListRecvMessages;
	private List<string> ListMessages;
	private List<string> ListServer;
	private e_state __current_state = e_state.INACTIVE;
	private float __timer = 0.0f;
	private string addr;
	private RandomMatchmaker myNetwork;
	private bool err = true;

	void Start()
	{
		while (this.err)
		{
			try
			{
				this.__objUDPClient = new UdpClient(this.__udp_port);
				this.ListRecvMessages = new Queue<string>();
				this.ListMessages = new List<string>();
				this.ListServer = new List<string>();
				this.myNetwork = GameObject.Find("Network").GetComponent<RandomMatchmaker>() as RandomMatchmaker;
				this.err = false;
			}
			catch (System.Exception e)
			{
				this.err = true;
				this.__udp_port++;
				Debug.LogError(e.Message);
			}
		}
		this.addr = Network.player.ipAddress;
	}

	void Update()
	{
		if (this.__current_state == e_state.ACTIVE)
		{
			this.__timer += Time.deltaTime;
			if (this.__timer > 60.0f)
			{
				this.__timer = 0.0f;
				this.Refresh();
			}
		}
	}

	public void SearchServer()
	{
		// 0 correspond to I research a server.
		this.SendUdpMessage("[Search]0");
	}

	public void SendUdpMessage(string str)
	{
		try
		{
			byte[] objByteMessageToSend = System.Text.Encoding.ASCII.GetBytes(str);
			IPEndPoint obj = new IPEndPoint(IPAddress.Broadcast, this.__udp_port);

			this.__objUDPClient.Send(objByteMessageToSend, objByteMessageToSend.Length, obj);
		}
		catch (System.Exception e)
		{
			Debug.LogError(e.ToString());
		}
	}

	public void StopBroadcast()
	{
		this.__timer = 0.0f;
		this.__objUDPClient.EnableBroadcast = false;
		this.__current_state = e_state.INACTIVE;
		this.ListRecvMessages.Clear();
		this.ListServer.Clear();
		this.ListMessages.Clear();
	}

	public void StartBroadcast()
	{
		this.ListRecvMessages.Clear();
		this.ListServer.Clear();
		this.ListMessages.Clear();
		this.__objUDPClient.EnableBroadcast = true;
		this.__current_state = e_state.ACTIVE;
		this.BeginSearch();
	}

	public List<string> StartSearch()
	{
		this.SearchServer();
		return this.ListServer;
	}

	public int WaitingMessage()
	{
		return this.ListRecvMessages.Count;
	}

	private void BeginSearch()
	{
		//this.__timer += 1;
		this.__objUDPClient.BeginReceive(new System.AsyncCallback(this.EndSearch), null);
	}

	private void EndSearch(System.IAsyncResult objResult)
	{
		IPEndPoint objSendersIPEndPoint = new IPEndPoint(IPAddress.Any, 0);
		byte[] objByteMessage = this.__objUDPClient.EndReceive(objResult, ref objSendersIPEndPoint);

		if (objByteMessage.Length > 0)
		{
			string message = System.Text.Encoding.ASCII.GetString(objByteMessage);
			if (!message.Contains("[Search]"))
				this.ListMessages.Add(message);
			else
			{
				if (message.Contains("[Search]0"))
				{
					if (this.myNetwork.eStatus == RandomMatchmaker.NetStatus.HOSTING)
					{
						string infos = "[Search]" + this.addr + " " + this.myNetwork.hostName + " " + RandomMatchmaker.playersList.Count + " / " + this.myNetwork.maxPlayerPerGame;
						this.SendUdpMessage(infos);
					}
				}
				else
				{
					if (this.myNetwork.eStatus == RandomMatchmaker.NetStatus.SEARCHING)
					{
						string convertMessage = message.Remove(0, 8);
						Debug.Log("Add serv " + convertMessage);
						if (!this.ListServer.Contains(convertMessage))
							this.ListServer.Add(convertMessage);
					}
				}
			}
		}
		if (this.__current_state == e_state.ACTIVE)
		{
			this.BeginSearch();
		}
	}

	public void Refresh()
	{
		this.__timer = 0.0f;
		this.Clear();
		this.SearchServer();
	}

	public void Clear()
	{
		this.ListMessages.Clear();
		this.ListRecvMessages.Clear();
		this.ListServer.Clear();
	}
}