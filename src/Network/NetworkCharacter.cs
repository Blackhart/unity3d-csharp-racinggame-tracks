﻿using UnityEngine;
using System.Collections;

public class NetworkCharacter : Photon.MonoBehaviour
{
	public int ID;
	private Vector3 direction;
	private float speed;
	private float boostSpeed;
	private float minSpeed;
	private float fwdForce;
	private Controlleur controlleur;
	private AnimationController animationControlleur;

	void Start()
	{
		this.controlleur = this.gameObject.GetComponent<Controlleur>();
		this.animationControlleur = this.gameObject.GetComponent<AnimationController>();
		this.minSpeed = this.controlleur.minSpeed;
	}

	// Update is called once per frame
	void Update()
	{
		if (!photonView.isMine && this.controlleur.noNetwork == false)
		{
			this.controlleur.curAngles.y = this.direction.y;
			this.speed = this.minSpeed + this.fwdForce;
			if (this.speed < this.minSpeed)
				this.speed = this.minSpeed;
			this.speed += this.boostSpeed;
			this.rigidbody.AddForce(this.controlleur.body.transform.forward * this.speed * 10, ForceMode.Force);
			//this.animationControlleur.animator.SetBool("MaxSpeed", this.speed >= 0.1f);
			this.animationControlleur.PlayAnim();
			//Debug.Log("pos: " + this.transform.position + " speed: " + this.speed);
		}
		//else
			//Debug.Log("my state: " + this.controlleur.state);
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			if (this.controlleur)
			{
				// We own this player: send the others our data
				this.direction = new Vector3(0, this.controlleur.curAngles.y, 0);
				//this.speed = this.gameObject.GetComponent<Controlleur>().currentSpeed;
				this.boostSpeed = this.controlleur.boostSpeed;
				this.fwdForce = Input.GetAxis("Forward") * this.controlleur.acceleration;
				this.controlleur.state = this.controlleur.state;
				stream.SendNext(this.direction);
				stream.SendNext(this.boostSpeed);
				stream.SendNext(this.fwdForce);
				stream.SendNext(this.controlleur.state);
			}
		}
		else
		{
			// Network player, receive data
			this.direction = (Vector3)stream.ReceiveNext();
			this.boostSpeed = (float)stream.ReceiveNext();
			this.fwdForce = (float)stream.ReceiveNext();
			if (this.controlleur)
				this.controlleur.state = (EState)stream.ReceiveNext();
			//this.speed = (float)stream.ReceiveNext();
		}
	}

	[RPC]
	void TaggedPlayer(int idPlayer)
	{ }
}
