﻿using UnityEngine;
using System.Collections;

public class Player
{
	public bool isReady;
	public int ID;
	public int PhotonID;
	public string characModel = "Scruffy";

	public Player(int PhotonID, int ID)
	{
		this.PhotonID = PhotonID;
		this.ID = ID;
	}

	// Use this for initialization
	void Start()
	{
		this.isReady = false;
	}

	// Update is called once per frame
	void Update()
	{

	}
}
