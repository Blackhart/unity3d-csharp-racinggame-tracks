﻿using UnityEngine;
using System.Collections.Generic;

public class RandomMatchmaker : MonoBehaviour
{
	public enum NetStatus
	{
		IDLE = 0,
		SEARCHING,
		HOSTING,
		INGAME
	};

	private enum Page
	{ 
		NONE = 0,
		ACCUEIL,
		CHANGECHARACTERSOLO,
		ROOM,
		CHANGECHARACTER,
		LOBBY
	};

	public Texture[] aTexture = new Texture[4];
	public static List<Player> playersList;
	public List<string> gamesList;
	public Lan_broadcast broadcaster;
	public NetStatus eStatus = NetStatus.IDLE;
	public string hostName;
	public int maxPlayerPerGame = 4;
	public static bool isInLobby;
	public static bool isNeedSetOtherPlayers = true;
	public static GameObject accueil;
	public static GameObject join;
	public static GameObject create;
	public static GameObject lobby;
	public static GameObject room;
	public static GameObject changeCharacter;
	public static GameObject changeCharacterSolo;
	public UILabel[] gamesLabel;
	public UILabel[] playersLabel;
	public UISprite[] playersPanel;
	public UILabel[] playersState;
	public UISprite[] gamesButton;
	public UILabel ipLabel;
	public UISprite buttonLaunch;
	public static LoadingScreen loadingSolo;
	public static LoadingScreen loadingMulti;
	public int inLoading = -1;

	private bool isInCharacterSelection = false;
	private float pictureWidth = 60.0f;
	private float spaceBetweenPicture = 15.0f;
	private string ip = "";
	private string[] aCharacModels;
	private Page oldPage;
	private Page newPage;
    private DJLoup themeSound;

	// Use this for initialization
	void Start()
	{
		RandomMatchmaker.accueil = GameObject.Find("Accueil");
		RandomMatchmaker.join = GameObject.Find("Join");
		RandomMatchmaker.create = GameObject.Find("Create");
		RandomMatchmaker.lobby = GameObject.Find("Lobby");
		RandomMatchmaker.room = GameObject.Find("Room");
		RandomMatchmaker.changeCharacter = GameObject.Find("ChangeCharacter");
		RandomMatchmaker.changeCharacterSolo = GameObject.Find("ChangeCharacterSolo");
		RandomMatchmaker.loadingSolo = GameObject.Find("LoadingSolo").GetComponent<LoadingScreen>();
		RandomMatchmaker.loadingMulti = GameObject.Find("Loading").GetComponent<LoadingScreen>();
		RandomMatchmaker.accueil.SetActive(false);
		RandomMatchmaker.join.SetActive(false);
		RandomMatchmaker.create.SetActive(false);
		RandomMatchmaker.lobby.SetActive(false);
		RandomMatchmaker.room.SetActive(false);
		RandomMatchmaker.loadingSolo.gameObject.SetActive(false);
		RandomMatchmaker.loadingMulti.gameObject.SetActive(false);
		RandomMatchmaker.changeCharacterSolo.SetActive(false);
		int count = this.gamesLabel.Length;
		for (int i = 0; i < count; i++)
		{
			this.gamesButton[i].gameObject.SetActive(false);
		}
		playersList = new List<Player>();
		gamesList = new List<string>();
		isInLobby = false;
		this.hostName = "toto";
		this.aCharacModels = new string[4];
		this.aCharacModels[0] = "Scruffy";
		this.aCharacModels[1] = "Squad";
		this.newPage = Page.ACCUEIL;
        themeSound = GameObject.Find("DJLoup").GetComponent<DJLoup>();
        themeSound.launchMenuThemeSong();
	}

	void OnGUI()
	{
		if (GameLogic.isLaunch == false)
		{
			GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		} 
	}

	void Update()
	{
		/*if (GameLogic.isLaunch)
		{
			if (Input.GetKeyDown(KeyCode.R))
			{
				GameLogic.SendRestart();
				GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
				for (int i = 0; i < GameLogic.nbPlayers; ++i)
				{
					GameLogic.SetPlayer(players[i], i);
				}
			}
		}*/

		if (GameLogic.isLaunch == true)
		{
			GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
			if (isNeedSetOtherPlayers == true && players.Length == GameLogic.nbPlayers)
			{
				this.broadcaster.StopBroadcast();
				for (int i = 0; i < GameLogic.nbPlayers; i++)
				{
					GameLogic.SetPlayer(players[i], i);
				}
				isNeedSetOtherPlayers = false;
				GameLogic.StartCountdown();
			}
			if (this.eStatus != NetStatus.INGAME)
			{
				this.eStatus = NetStatus.INGAME;
			}
			RandomMatchmaker.room.SetActive(false);
		}

		if (isInLobby == true && PhotonNetwork.connectionStateDetailed != PeerState.Joined)
		{
			RandomMatchmaker.lobby.SetActive(true);
			if (this.oldPage != this.newPage)
			{
				RandomMatchmaker.lobby.GetComponent<SelectItem>().Select();
				this.oldPage = this.newPage;
			}
			this.DrawInfoLobby();
			RandomMatchmaker.accueil.SetActive(false);
			RandomMatchmaker.join.SetActive(false);
			RandomMatchmaker.create.SetActive(false);
			RandomMatchmaker.room.SetActive(false);
		}
		else if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
		{
			if (this.isInCharacterSelection == false)
			{
				RandomMatchmaker.accueil.SetActive(true);
				if (this.oldPage != this.newPage)
				{
					RandomMatchmaker.accueil.GetComponent<SelectItem>().Select();
					this.oldPage = this.newPage;
				}
				RandomMatchmaker.lobby.SetActive(false);
				RandomMatchmaker.room.SetActive(false);
				RandomMatchmaker.changeCharacterSolo.SetActive(false);
				for (int i = 0; i < 5; i++)
				{
					this.gamesButton[i].gameObject.SetActive(false);
				}
			}
			else
			{
				RandomMatchmaker.changeCharacterSolo.SetActive(true);
				if (this.oldPage != this.newPage)
				{
					RandomMatchmaker.changeCharacterSolo.GetComponent<SelectItem>().Select();
					this.oldPage = this.newPage;
				}
				RandomMatchmaker.accueil.SetActive(false);
				RandomMatchmaker.join.SetActive(false);
				RandomMatchmaker.create.SetActive(false);
			}
		}
		else if (PhotonNetwork.connectionStateDetailed == PeerState.Joined && GameLogic.isLaunch == false)
		{
			if (isInLobby == true && PhotonNetwork.player.ID == GameLogic.playerWhoIsIt)
			{
				RandomMatchmaker.playersList.Clear();
				PhotonNetwork.Disconnect();
			}
			/*else if (this.isInCharacterSelection)
			{
				RandomMatchmaker.changeCharacter.SetActive(true);
				if (this.oldPage != this.newPage)
				{
					RandomMatchmaker.changeCharacter.GetComponent<SelectItem>().Select();
					this.oldPage = this.newPage;
				}
				RandomMatchmaker.room.SetActive(false);
			}*/
			else
			{
				isInLobby = false;
				RandomMatchmaker.room.SetActive(true);
				if (this.oldPage != this.newPage)
				{
					RandomMatchmaker.room.GetComponent<SelectItem>().Select();
					this.oldPage = this.newPage;
				}
				this.DrawInfoRoom(RandomMatchmaker.playersList);
			}
			RandomMatchmaker.accueil.SetActive(false);
			RandomMatchmaker.join.SetActive(false);
			RandomMatchmaker.create.SetActive(false);
			RandomMatchmaker.lobby.SetActive(false);
			for (int i = 0; i < 5; i++)
			{
				this.gamesButton[i].gameObject.SetActive(false);
			}
		}
		if (this.inLoading == 0)
		{
			if (RandomMatchmaker.loadingSolo.IsLoaded() == true)
			{
				Destroy(RandomMatchmaker.changeCharacterSolo);
				this.LaunchGameSolo();
			}
			else
			{
				RandomMatchmaker.changeCharacterSolo.SetActive(false);
				RandomMatchmaker.loadingSolo.gameObject.SetActive(true);
			}
		}
		else if (this.inLoading == 1)
		{
			if (RandomMatchmaker.loadingMulti.IsLoaded() == true)
			{
				this.LaunchGame();
			}
			else
			{
				RandomMatchmaker.room.SetActive(false);
				RandomMatchmaker.loadingMulti.gameObject.SetActive(true);
			}
		}
		if (RandomMatchmaker.loadingMulti.gameObject.activeInHierarchy == true)
		{
			RandomMatchmaker.room.SetActive(false);
		}
	}

	public void ClickOnQuit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void ClickOnMulti()
	{
		RandomMatchmaker.join.SetActive(true);
		RandomMatchmaker.create.SetActive(true);
	}

	public void ClickOnJoin()
	{
		isInLobby = true;
		this.eStatus = NetStatus.SEARCHING;
		this.broadcaster.StartBroadcast();
		this.gamesList = this.broadcaster.StartSearch();
		this.newPage = Page.LOBBY;
	}

	public void ClickOnCreate()
	{
		RandomMatchmaker.room.GetComponent<ChangeCharacter>().UpdateCharacterSelection(-1);
		PhotonNetwork.Connect(Network.player.ipAddress, 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.broadcaster.StartBroadcast();
		this.eStatus = NetStatus.HOSTING;
		this.newPage = Page.ROOM;
	}

	public void ClickOnSolo()
	{
		RandomMatchmaker.changeCharacterSolo.GetComponent<ChangeCharacter>().UpdateCharacterSelection(-1);
		this.isInCharacterSelection = true;
		this.newPage = Page.CHANGECHARACTERSOLO;
	}

	public void ClickOnBackSolo()
	{
		this.isInCharacterSelection = false;
		this.newPage = Page.ACCUEIL;
	}

	private void LaunchGameSolo()
	{
		this.inLoading = -1;
		RandomMatchmaker.loadingSolo.ResetTimer();
		DontDestroyOnLoad(GameObject.Find("ApplicationHandler"));
		Application.LoadLevel(1);
	}

	public void ClickOnLaunchSolo()
	{
		this.inLoading = 0;
	}

	public void ClickOnFirstSolo()
	{
		GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().model = "Scruffy";
		RandomMatchmaker.changeCharacterSolo.GetComponent<ChangeCharacter>().UpdateCharacterSelection(0);
	}

	public void ClickOnSecondSolo()
	{
		GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().model = "Squad";
		RandomMatchmaker.changeCharacterSolo.GetComponent<ChangeCharacter>().UpdateCharacterSelection(1);
	}

	public void ClickOnThirdSolo()
	{
		Debug.Log("Character incoming");
		//GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().model = "Squad";
	}

	public void ClickOnFourthSolo()
	{
		Debug.Log("Character incoming");
		//GameObject.Find("ApplicationHandler").GetComponent<ApplicationHandler>().model = "Squad";
	}

	void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom(null);
	}

	void OnJoinedLobby()
	{
		PhotonNetwork.JoinRandomRoom();
	}

	public void ClickOnBack()
	{
		this.ipLabel.text = "";
		isInLobby = false;
		this.broadcaster.StopBroadcast();
		this.newPage = Page.ACCUEIL;
	}

	public void ClickOnJoinIP()
	{
		RandomMatchmaker.room.GetComponent<ChangeCharacter>().UpdateCharacterSelection(-1);
		PhotonNetwork.Connect(this.ip, 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.newPage = Page.ROOM;
	}

	public void ClickOnRefresh()
	{
		this.gameObject.GetComponent<Lan_broadcast>().Refresh();
	}

	public void ClickOnFirstGame()
	{
		string[] str = this.gamesList[0].Split(' ');
		PhotonNetwork.Connect(str[0], 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.newPage = Page.ROOM;
	}

	public void ClickOnSecondGame()
	{
		string[] str = this.gamesList[1].Split(' ');
		PhotonNetwork.Connect(str[0], 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.newPage = Page.ROOM;
	}

	public void ClickOnThirdGame()
	{
		string[] str = this.gamesList[2].Split(' ');
		PhotonNetwork.Connect(str[0], 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.newPage = Page.ROOM;
	}

	public void ClickOnFourthGame()
	{
		string[] str = this.gamesList[3].Split(' ');
		PhotonNetwork.Connect(str[0], 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.newPage = Page.ROOM;
	}

	public void ClickOnFifthGame()
	{
		string[] str = this.gamesList[4].Split(' ');
		PhotonNetwork.Connect(str[0], 5055, "f6065adc-6780-4bde-a618-78979b16c3c8", "0.1");
		this.newPage = Page.ROOM;
	}

	void DrawInfoLobby()
	{
		string[] str;
		for (int i = 0; i < gamesList.Count; ++i)
		{
			str = this.gamesList[i].Split(' ');
			if (i < 5)
			{
				this.gamesLabel[i].text = str[1] + " " + str[2] + " " + str[3] + " " + str[4];
				this.gamesButton[i].gameObject.SetActive(true);
			}
		}
		this.ip = this.ipLabel.text;
	}

	public void BackToMenu()
	{
		this.broadcaster.StopBroadcast();
		RandomMatchmaker.playersList.Clear();
		RandomMatchmaker.isNeedSetOtherPlayers = true;
		GameLogic.isLaunch = false;
		GameLogic.isInPreRace = false;
		GameLogic.isRaceStart = false;
		GameLogic.countdown = 3.0f;
		this.eStatus = NetStatus.IDLE;
		PhotonNetwork.Disconnect();
		this.newPage = Page.ACCUEIL;
	}

	public void ClickOnBackRoom()
	{
		this.broadcaster.StopBroadcast();
		RandomMatchmaker.playersList.Clear();
		PhotonNetwork.Disconnect();
		this.newPage = Page.ACCUEIL;
	}

	public void ClickOnReady()
	{
		int myID = GameLogic.GetPlayerByID(PhotonNetwork.player.ID).ID;
		RandomMatchmaker.playersList[myID - 1].isReady = !RandomMatchmaker.playersList[myID - 1].isReady;
		GameLogic.SendChangeState(myID - 1, RandomMatchmaker.playersList[myID - 1].isReady); 
	}

	public void ClickOnChangeCharacter()
	{
		this.isInCharacterSelection = true;
		this.newPage = Page.CHANGECHARACTER;
	}

	private void LaunchGame()
	{
		this.inLoading = -1;
		GameLogic.SendLaunchGame(RandomMatchmaker.playersList); 
	}

	public void ClickOnLaunch()
	{
		this.inLoading = 1;
		GameLogic.SendLoadingScreen(); 
	}

	public void DrawInfoRoom(List<Player> playerList)
	{
		int playerReady = 0;
		int i;

		this.buttonLaunch.gameObject.SetActive(false);
		for (i = 0; i < playerList.Count; ++i)
		{
			string str = "Player " + (i + 1);
			string state;

			this.playersPanel[i].gameObject.SetActive(true);
			if (playerList[i].isReady == false)
			{
				state = "";
				if (playerList[i].characModel == "Scruffy")
				{
					this.playersPanel[i].spriteName = "ROOM_MULTI_PLAYER_NOTREADY_SCRUFFY";
				}
				else
				{
					this.playersPanel[i].spriteName = "ROOM_MULTI_PLAYER_NOTREADY_V12";
				}
			}
			else
			{
				state = "READY !";
				if (playerList[i].characModel == "Scruffy")
				{
					this.playersPanel[i].spriteName = "ROOM_MULTI_PLAYER_READY_SCRUFFY";
				}
				else
				{
					this.playersPanel[i].spriteName = "ROOM_MULTI_PLAYER_READY_V12";
				}
				playerReady += 1;
			}
			this.playersLabel[i].text = str;
			this.playersState[i].text = state;
		}
		while (i < 4)
		{
			this.playersLabel[i].text = "";
			this.playersState[i].text = "";
			this.playersPanel[i].gameObject.SetActive(false);
			i++;
		}
		if (playerReady == playerList.Count && playerList.Count > 0 && PhotonNetwork.player.ID == GameLogic.playerWhoIsIt)
		{
			this.buttonLaunch.gameObject.SetActive(true);
		}
	}

	public void ClickOnBackCharacter()
	{
		this.isInCharacterSelection = false;
		this.newPage = Page.ROOM;
	}

	public void ClickOnFirst()
	{
		int myID = GameLogic.GetPlayerByID(PhotonNetwork.player.ID).ID;

		GameLogic.GetPlayerByID(PhotonNetwork.player.ID).characModel = this.aCharacModels[0];
		GameLogic.SendChangePerso(myID - 1, this.aCharacModels[0]);
		RandomMatchmaker.room.GetComponent<ChangeCharacter>().UpdateCharacterSelection(0);
		this.isInCharacterSelection = false;
		this.newPage = Page.ROOM;
	}

	public void ClickOnSecond()
	{
		int myID = GameLogic.GetPlayerByID(PhotonNetwork.player.ID).ID;

		GameLogic.GetPlayerByID(PhotonNetwork.player.ID).characModel = this.aCharacModels[1];
		GameLogic.SendChangePerso(myID - 1, this.aCharacModels[1]);
		RandomMatchmaker.room.GetComponent<ChangeCharacter>().UpdateCharacterSelection(1);
		this.isInCharacterSelection = false;
		this.newPage = Page.ROOM;
	}

	public void ClickOnThird()
	{
		Debug.Log("Character incoming");
		/*int myID = GameLogic.GetPlayerByID(PhotonNetwork.player.ID).ID;

		GameLogic.GetPlayerByID(PhotonNetwork.player.ID).characModel = this.aCharacModels[2];
		GameLogic.SendChangePerso(myID - 1, this.aCharacModels[0]);
		this.isInCharacterSelection = false;*/
	}

	public void ClickOnFourth()
	{
		Debug.Log("Character incoming");
		/*int myID = GameLogic.GetPlayerByID(PhotonNetwork.player.ID).ID;

		GameLogic.GetPlayerByID(PhotonNetwork.player.ID).characModel = this.aCharacModels[3];
		GameLogic.SendChangePerso(myID - 1, this.aCharacModels[0]);
		this.isInCharacterSelection = false;*/
	}

	public void ClickOnBackResults()
	{
		this.oldPage = Page.NONE;
		this.newPage = Page.ROOM;
	}
}
