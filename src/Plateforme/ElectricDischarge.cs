using UnityEngine;
using System.Collections;

public class ElectricDischarge : Photon.MonoBehaviour
{
	#region Public field
	public float m_MinTimer; // en s
	public float m_MaxTimer; // en s
	public float m_IndicCame; // Indic apparition
	public Color m_IndicDechMat; // Blink
	public GameObject m_DechFX; // Fx decharge
	public string eventName;
	public FmodEventAsset asset;
	public FmodEventAudioSource source;
	public GameObject drone;

	#endregion

	#region Private field
	private float _time;
	private float _timeRand;
	private PlateformData _plateformData;
	private Color _mainColor;
	#endregion

	#region Init

	void Awake()
	{
		this.source = this.GetComponent<FmodEventAudioSource>();
		this.source.playOnAwake = false; 
		if (Moteur.CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
		_mainColor = drone.renderer.materials[1].color;
	}

	public void Start()
	{
		if (GameObject.FindGameObjectWithTag("Player"))
		{
			if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>())
			{
				if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>().noNetwork)
				{
                    Init();
					ResetTime();
					SetRand(Random.Range(m_MinTimer, m_MaxTimer));
				}
			}
		}
	}

	public void Init()
	{
		_plateformData = GetComponent<PlateformData>();
		if (PhotonNetwork.isMasterClient)
		{
			photonView.RPC("ResetTime", PhotonTargets.All);
			photonView.RPC("SetRand", PhotonTargets.All, Random.Range(m_MinTimer, m_MaxTimer));
		}
	}
	#endregion

	#region Update
	void Update()
    {
		if (_plateformData && _plateformData.m_Player)
		{
			if (PhotonNetwork.isMasterClient)
				photonView.RPC("SetTime", PhotonTargets.All, _time + Time.deltaTime);
			else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>().noNetwork)
				SetTime(_time + Time.deltaTime);
			if (_time >= _timeRand - m_IndicCame && _time < _timeRand)
			{
				if (_time % 0.2f >= 0.1f)
					drone.renderer.materials[1].color = m_IndicDechMat;
				else
					drone.renderer.materials[1].color = _mainColor;
			}
			else if (_time > _timeRand)
			{
				if (PhotonNetwork.isMasterClient)
					photonView.RPC("SendDischarge", PhotonTargets.All);
				else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>().noNetwork)
					Discharge();
			}
		}
		else
		{
			if (PhotonNetwork.isMasterClient)
			{
				photonView.RPC("SetRand", PhotonTargets.All, Random.Range(m_MinTimer, m_MaxTimer));
				photonView.RPC("ResetTime", PhotonTargets.All);
			}
			else if (GameObject.FindGameObjectWithTag("Player"))
			{
				if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>())
				{
					if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>().noNetwork)
					{
						SetRand(Random.Range(m_MinTimer, m_MaxTimer));
						ResetTime();
					}
				}
			}
			drone.renderer.materials[1].color = _mainColor;
		}
	}
	#endregion

	#region Reseau
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
	}

	[RPC]
	void SendDischarge()
	{
		Discharge();
	}

	[RPC]
	void SetTime(float time)
	{
		_time = time;
	}

	[RPC]
	void ResetTime()
	{
		_time = 0.0f;
	}

	[RPC]
	void SetRand(float rand)
	{
		_timeRand = rand;
	}
	#endregion

	#region GameLogic
	void Discharge()
	{
		_plateformData.m_Player.GetComponent<AnimationController>().hit = true;
		_plateformData.m_Player.GetComponent<FXManager>().discharge.particleSystem.Play();
		SetRand(Random.Range(m_MinTimer, m_MaxTimer));
		drone.renderer.materials[1].color = _mainColor;
		this.source.Play();
		Instantiate(m_DechFX, transform.position, Quaternion.identity);
		_plateformData.m_Player.GetComponent<Filin>()._filinState = eFilinState._CUT_;
		_plateformData.m_Player = null;
		if (PhotonNetwork.isMasterClient)
		{
			photonView.RPC("ResetTime", PhotonTargets.All);
			photonView.RPC("SetRand", PhotonTargets.All, Random.Range(m_MinTimer, m_MaxTimer));
		}
		else if (GameObject.FindGameObjectWithTag("Player").GetComponent<Controlleur>().noNetwork)
		{
			ResetTime();
			SetRand(Random.Range(m_MinTimer, m_MaxTimer));
		}
	}
	#endregion
}

