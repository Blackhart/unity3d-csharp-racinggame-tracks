﻿using UnityEngine;
using System.Collections;

public class movePlateform : MonoBehaviour
{
	public float speedPlateforme = 20f;
    [HideInInspector]
    public float distanceParcouru;
	public bool isLaunch = false;

    private Spline.Path mPath = new Spline.Path();

	// Use this for initialization
	void Start()
	{
		distanceParcouru = 0.0f;
        transform.position = iTweenPath.GetPath("plateformRail")[0];
        mPath = iTweenPath.GetPath("plateformRail");
	}

	// Update is called once per frame
	void Update()
	{
		if (this.isLaunch == true)
		{
            Quaternion  lQuatern = new Quaternion();
            transform.position = Spline.MoveOnPath(mPath, transform.position, ref distanceParcouru, ref lQuatern, speedPlateforme);
            transform.rotation = lQuatern;
		}
	}
}
