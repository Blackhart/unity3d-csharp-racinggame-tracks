﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DJLoup : MonoBehaviour
{
    public FmodEventAsset asset;
    public FmodEventAudioSource src;
    public List<string> soundList;

    private void Start()
    {
    }

    private void launchTheme(int pIndex)
    {
        if (CreateEventInstance(asset.GetEventWithName(soundList[pIndex]), src) == false)
            Debug.LogError("Can't load event: " + soundList[pIndex]);
        else
            src.Play();
    }

    public void launchMenuThemeSong()
    {
        if (src)
            src.Stop();
        launchTheme(1);
    }

    public void launchGameThemeSong()
    {
        if (src)
            src.Stop();
        launchTheme(0);
    }

    public static bool CreateEventInstance(FmodEvent srcEvent, FmodEventAudioSource source)
    {
        if (srcEvent == null)
        {
            Debug.LogError("srcEvent is null");
            return false;
        }
        FmodEventAudioClip clip = ScriptableObject.CreateInstance<FmodEventAudioClip>();

#if UNITY_EDITOR
        FmodEvent evt = ScriptableObject.CreateInstance("FmodEvent") as FmodEvent;
        evt.Initialize(srcEvent);
        clip.Initialize(evt);
#else
		clip.Initialize(srcEvent);
#endif
        source.eventClip = clip;
        return true;
    }
}
