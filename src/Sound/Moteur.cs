﻿using UnityEngine;
using System.Collections;

public class Moteur : MonoBehaviour
{
	public int rpmWithoutBoost;
	public int maxRPM;
	public int maxSpeed;
	public int maxBoost;
	public string eventName;
	public FmodEventAsset asset;

	public FmodEventAudioSource source;
	private Controlleur controller;

	void Awake()
	{
		this.source = this.GetComponent<FmodEventAudioSource>();
		this.source.playOnAwake = false; 
	}

	// Use this for initialization
	void Start()
	{
		if (CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
		else
		{
			this.source.Play();
		}
		this.controller = this.GetComponent<Controlleur>();
	}

	// Update is called once per frame
	void Update()
	{
		if (EndRace.inResult == false)
		{
			int value = this.rpmWithoutBoost / this.maxSpeed;
			int boost = (this.maxRPM - this.rpmWithoutBoost) / this.maxBoost;

			this.source.SetParameterValue("rpm", value * Mathf.Clamp(this.controller.currentSpeed, 0, this.maxSpeed) + boost * this.controller.boostSpeed);
		}
		else
		{
			this.source.Stop();
		}
	}

	public static bool CreateEventInstance(FmodEvent srcEvent, FmodEventAudioSource source)
	{
		if (srcEvent == null)
		{
			Debug.LogError("srcEvent is null");
			return false;
		}
		FmodEventAudioClip clip = ScriptableObject.CreateInstance<FmodEventAudioClip>();

#if UNITY_EDITOR
		FmodEvent evt = ScriptableObject.CreateInstance("FmodEvent") as FmodEvent;
		evt.Initialize(srcEvent);
		clip.Initialize(evt);
#else
		clip.Initialize(srcEvent);
#endif
		source.eventClip = clip;
		return true;
	}
}
