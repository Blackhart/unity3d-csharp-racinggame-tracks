﻿using UnityEngine;
using System.Collections;

public class TriggerSound : MonoBehaviour
{
	public string eventName;
	public FmodEventAsset asset;
	private FmodEventAudioSource source;

	void Awake()
	{
		this.source = this.GetComponent<FmodEventAudioSource>();
		this.source.playOnAwake = false;
	}

	void Start()
	{
		if (Moteur.CreateEventInstance(this.asset.GetEventWithName(this.eventName), this.source) == false)
			Debug.LogError("Can't load event: " + this.eventName);
	}

	void OnTriggerEnter()
	{
		this.source.Play();
	}
}
